
```bash
  _____   ______  _______    __      __ ______ __   __
 |  __ \ |  ____||__   __|   \ \    / /|  ____|\ \ / /
 | |  | || |__      | | ______\ \  / / | |__    \ V / 
 | |  | ||  __|     | ||______|\ \/ /  |  __|    > <  
 | |__| || |        | |         \  /   | |____  / . \ 
 |_____/ |_|        |_|          \/    |______|/_/ \_\

```

**DFT** based **V**isualization and parameter **EX**traction tool.
This tool can be used to visualize and extract parameters from
DFT calculations.
Right now the code supports `VASP`. The functions used are generic
enough to be extended to other DFT tools as well.

If available, the code uses `python` to process vasp output XML files.
For larger XML files this gives significant performance improvement.

## Table of contents

* [Band structure plot](#markdown-header-band-structure-plot)
* [DOS plot](#markdown-header-dos-plot)
* [Effective mass calculator](#markdown-header-effective-mass-calculator)


Band structure plot
--------------------

supported modes: `default`, `ionic`, `orbital`, 
`sigma-pi`, `t2g-eg` and `s-contrib`.

The `ionic` and `orbital` mode plots the ionic and orbital
composition in the band structure.
The `sigma-pi` mode plots the in-plane (sigma) and out-of-plane (pi) contribution
of the p-orbital.
The `t2g-eg` mode plots the contribution of `t2g` and `eg` bands of the
d-orbital.
The `s-contrib` mode plots contribution of s-orbital in the band structure.

For any mode specify energy range from `Emin` to `Emax` to plot.

### Generic band structure of a single layer PtSe2
![Band structure plot](images/bandstructure.png)

### Ionic/atomic composition of a single layer PtSe2
![Ionic composition plot](images/ionic_composition.png)

### Orbital composition of a single layer PtSe2
![Orbital composition plot](images/orbital_composition.png)

### Contribution of s orbital in band structure of a single layer PtSe2
![Contribution of s-orbital](images/contribution_s_orbital.png)

### Contribution of p orbital in band structure of a single layer PtSe2
![Contribution of p-orbital](images/contribution_p_orbital.png)

### Contribution of d orbital in band structure of a single layer PtSe2
![Contribution of d-orbital](images/contribution_d_orbital.png)


DOS plot
--------------------

Supported modes: `total`, `ionic`, `orbital` and `d-contrib`.

The `total` mode plots the total DOS of the given energy range.
The `ionic` and `orbital` mode plots the ionic and orbital
composition in the band structure.
The `d-contrib` mode plots contribution of individual d-orbital 
in the band structure.

For any mode specify energy range from `Emin` to `Emax` to plot.

### Total DOS for an AFM CrSb
![Total DOS](images/total_dos.png)

### Atomic contribution of DOS for an AFM CrSb
![Atomic DOS](images/atom_resolved_dos.png)

### Orbital contribution of DOS for an AFM CrSb
![Orbital DOS](images/orbital_resolved_dos.png)


Effective mass calculator
--------------------

Supported modes: `generic`, `kp-gen`, `het-kp-gen`, `mstar-calc`,
`het-mstar-calc` and `z-dir`.

For the `generic` mode the calculator uses the band structure to get an
estimate of effective mass.
`kp-gen` mode generates `k-points` for effective mass calculation 
from the band structure calculation along kx and ky. 
The output can be written as `KPOINTS` file
or a mat file.
`het-kp-gen` mode generates `k-points` for effective mass calculation from 
the band structure of heterostructure. This mode takes symbol of element
to identify the band of which to calculate effective mass.
`mstar-calc` mode calculates the effective mass from the output of calculation
done using `KPOINTS` file of `kp-gen`.
`het-mstar-calc` mode calculates the effective mass from the output of calculation
done using `KPOINTS` file of `het-kp-gen`.
`z-dir` mode does all the same calculations along the `z-direction`.

Necessary inputs are:
`mstar_prop.cut_th`: this is the threshold in meV to cut off from the
`maxima` or `minima`.
`mstar_prop.degrees`: this is the vector with the order of polynomials to
fit with.
`mstar_prop.split_th`: for the spin-polarized or SOC case, this is the
splitting threshold in meV. If the splitting is higher than the given
cutoff, then the code will calculate m* of the band below VB and the band
above CB.

### Effective mass fit for an AFM CrSb
![Effective mass](images/effective_mass_fit.png)

