function DFT_VEX

keySet =   {'1','2','3'};
valueSet = {'Band structure plot','DOS plot', 'effective mass calculation'};
mapObj = containers.Map(keySet,valueSet);

mat_file='all_data.mat';

addpath(genpath('src'));

helper_header();
MainMenu(mapObj, mat_file);

end

function MainMenu(mmMap, mat_file)

ProcessFile(mat_file);

choice = helper_print_menu(mmMap, 'main menu');

if strcmp(choice,'e')
    return;
elseif isempty(choice) || strcmp(choice,'1') % first one is always the default one
    BandStructureMenu(mmMap, mat_file)
elseif strcmp(choice,'2')
    DOSMenu(mmMap, mat_file)
elseif strcmp(choice,'3')
    EffectiveMassMenu(mmMap, mat_file)    
else
    warning(' Invalid input. Try again.');
    MainMenu(mmMap);    
end

end

function BandStructureMenu(mmMap, mat_file)

keySet =   {'1','2','3','4','5','6'};
valueSet = {'plain Band structure','ionic contribution', 'orbital contribution'...
    's-orbital contribution','p-orbital contribution', 'd-orbital contribution'};
bsMap = containers.Map(keySet,valueSet);

choice = helper_print_menu(bsMap, 'band structure menu');

if strcmp(choice,'e')
    MainMenu(mmMap)
else
    [format, dpi] = FigureMenu();
    
    if isempty(format) || isempty(dpi)
        return;
    end
    
    fig_prop.format=format;
    fig_prop.dpi=dpi;
    fig_prop.Linewidth=2.0;
    
    [Emin, Emax] = EnergyRangeMenu();
    
    if isempty(Emin) || isempty(Emax)
        return;
    end
    
    bs_prop.Emin= Emin;
    bs_prop.Emax= Emax;
    
    procar_prop.Emin = Emin;
    procar_prop.Emax = Emax;
    
    if isempty(choice)
        
        plot_bandstructure(mat_file, fig_prop, bs_prop)
        
    elseif isKey(bsMap, choice) && strcmp(choice,'1')
        
        plot_bandstructure(mat_file, fig_prop, bs_prop)
        
    elseif isKey(bsMap, choice) && strcmp(choice,'2')
        
        procar_prop.procar_flag='ionic';
        plot_procar(mat_file, procar_prop, fig_prop)
        
    elseif isKey(bsMap, choice) && strcmp(choice,'3')
        
        procar_prop.procar_flag='orbital';
        plot_procar(mat_file, procar_prop, fig_prop)
        
    elseif isKey(bsMap, choice) && strcmp(choice,'4')
        
        procar_prop.procar_flag='s-contrib';
        plot_procar(mat_file, procar_prop, fig_prop)
        
    elseif isKey(bsMap, choice) && strcmp(choice,'5')
        
        procar_prop.procar_flag='sigma-pi';
        plot_procar(mat_file, procar_prop, fig_prop);
        
    elseif isKey(bsMap, choice) && strcmp(choice,'6')
        
        procar_prop.procar_flag='t2g-eg';
        plot_procar(mat_file, procar_prop, fig_prop)  
        
    else
        warning(' Invalid input. Try again.');
        BandStructureMenu(mmMap, mat_file)
    end
end

end

function DOSMenu(mmMap, mat_file)

keySet =   {'1','2','3'};
valueSet = {'total DOS','ionic DOS', 'orbital DOS'};
dosMap = containers.Map(keySet,valueSet);

choice = helper_print_menu(dosMap, 'DOS menu');

if strcmp(choice,'e')
    MainMenu(mmMap)
else
    [format, dpi] = FigureMenu();
    
    if isempty(format) || isempty(dpi)
        return;
    end
    
    fig_prop.format=format;
    fig_prop.dpi=dpi;
    fig_prop.Linewidth=2.0;
    
    [Emin, Emax] = EnergyRangeMenu();
    
    if isempty(Emin) || isempty(Emax)
        return;
    end
    
    dos_prop.Emin= Emin;
    dos_prop.Emax= Emax;
    
    dos_max = DOSMaxMenu();
    
    dos_prop.dos_max= dos_max;
    
    if isempty(choice) || strcmp(choice,'1')
        
        dos_prop.dos_flag='total';        
        plot_doscar(mat_file, dos_prop, fig_prop)
        
    elseif isKey(dosMap, choice) && strcmp(choice,'2')
        
        dos_prop.dos_flag='ionic';
        plot_doscar(mat_file, dos_prop, fig_prop)
        
    elseif isKey(dosMap, choice) && strcmp(choice,'3')
        
        dos_prop.dos_flag='orbital';
        plot_doscar(mat_file, dos_prop, fig_prop)
        
    else
        warning(' Invalid input. Try again.');
        DOSMenu(mmMap, mat_file)
    end
end

end

function EffectiveMassMenu(mmMap, mat_file)

keySet =   {'1','2','3','4','5'};
valueSet = {'m* from band structure','generate kpoints for m* calc', ...
    'final m* calculation', 'generate kpoints for heterostructure'...
    'calculate m* for heterostructure'};
mstarMap = containers.Map(keySet,valueSet);

choice = helper_print_menu(mstarMap, 'effective mass menu');

if strcmp(choice,'e')
    MainMenu(mmMap)
else
    
    mstar_prop.data_save=true;
    
    fig_prop= struct;
    
    [cut_th, order, split_th] = mstarCalcMenu();

    if isempty(cut_th) || isempty(order) || isempty(split_th)
        return;
    end
    

    if isempty(choice) || strcmp(choice,'1')
        
        [format, dpi] = FigureMenu();
        
        if isempty(format) || isempty(dpi)
            return;
        end
    
        fig_prop.format=format;
        fig_prop.dpi=dpi;
        fig_prop.Linewidth=2.0;
        
        mstar_prop.mstar_mode='generic';
        effective_mass(mat_file, mstar_prop, fig_prop);
        
    elseif isKey(mstarMap, choice) && strcmp(choice,'2')
       
        [no_kpoints, write_mode] = kpointsMenu();

        if isempty(no_kpoints) || isempty(write_mode)
            return;
        end
        
        mstar_prop.mstar_mode='kp-gen';
        mstar_prop.no_kpoints = no_kpoints;
        mstar_prop.write_mode = write_mode;
        mstar_prop.cut_th = cut_th; % in meV
        mstar_prop.split_th = split_th; % in meV
        mstar_prop.degrees = order; % in meV
        
        effective_mass(mat_file, mstar_prop, fig_prop);
        
    elseif isKey(mstarMap, choice) && strcmp(choice,'3')
        
        [format, dpi] = FigureMenu();
        
        if isempty(format) || isempty(dpi)
            return;
        end
    
        fig_prop.format=format;
        fig_prop.dpi=dpi;
        fig_prop.Linewidth=2.0;
       
        mstar_prop.cut_th = cut_th; % in meV
        mstar_prop.degrees = order;
        mstar_prop.mstar_mode='mstar-calc';
        mstar_prop.split_th = split_th; % in meV
        mstar_prop.degrees = order; % in meV
        
        effective_mass(mat_file, mstar_prop, fig_prop);
        
    elseif isKey(mstarMap, choice) && strcmp(choice,'4')
        
        [no_kpoints, write_mode] = kpointsMenu();
        
        if isempty(no_kpoints) || isempty(write_mode)
            return;
        end
        
        mat_sym = MatSym(mat_file);
        
        if isempty(mat_sym)
            return;
        end
        
        mstar_prop.mat_sym={mat_sym};
        mstar_prop.no_kpoints = no_kpoints;
        mstar_prop.write_mode = write_mode;
        mstar_prop.mstar_mode='het-kp-gen';
        mstar_prop.cut_th = cut_th; % in meV
        mstar_prop.split_th = split_th; % in meV
        mstar_prop.degrees = order; % in meV
        
        effective_mass(mat_file, mstar_prop, fig_prop);
        
    elseif isKey(mstarMap, choice) && strcmp(choice,'5')
        
        [format, dpi] = FigureMenu();
        
        if isempty(format) || isempty(dpi)
            return;
        end
    
        fig_prop.format=format;
        fig_prop.dpi=dpi;
        fig_prop.Linewidth=2.0;
        
        [cut_th, order] = mstarCalcMenu();
        
        if isempty(cut_th) || isempty(order)
            return;
        end
        
        mat_sym = MatSym(mat_file);
        
        if isempty(mat_sym)
            return;
        end
        
        mstar_prop.mat_sym={mat_sym};
        mstar_prop.cut_th = cut_th; % in meV
        mstar_prop.degrees = order;
        mstar_prop.mstar_mode='het-mstar-calc';
        mstar_prop.split_th = split_th; % in meV
        
        effective_mass(mat_file, mstar_prop, fig_prop);
        
    else
        warning(' Invalid input. Try again.');
        EffectiveMassMenu(mmMap, mat_file)
    end
end

end

function helper_header()

fprintf('\n\n')
fprintf('  _____   ______  _______    __      __ ______ __   __\n');
fprintf(' |  __ \\ |  ____||__   __|   \\ \\    / /|  ____|\\ \\ / /\n');
fprintf(' | |  | || |__      | | ______\\ \\  / / | |__    \\ V / \n');
fprintf(' | |  | ||  __|     | ||______|\\ \\/ /  |  __|    > <  \n');
fprintf(' | |__| || |        | |         \\  /   | |____  / . \\ \n');
fprintf(' |_____/ |_|        |_|          \\/    |______|/_/ \\_\\\n');
fprintf('\n\n')

end

function helper_traverse_map(mapObj)

exit_opt = {'e','exit'};

count = 1;
for k=keys(mapObj)
    
    if count == 1
        fprintf('%5s: %s [default]\n', k{1}, mapObj(k{1}));
    else
        fprintf('%5s: %s\n', k{1}, mapObj(k{1}));
    end
    
    count = count + 1;
end

fprintf('%5s: %s\n',exit_opt{1},exit_opt{2});

fprintf('\n');

end

function choice = helper_print_menu(mapObj, menu_str)

fprintf(' Choose an option (%s):\n\n', menu_str);

helper_traverse_map(mapObj);

prompt= sprintf(' [Enter for default] DFT-VEX>> ');

choice = input(prompt,'s');
fprintf('\n');

end

function [format, dpi] = FigureMenu()

format = formatMenu();

if isempty(format)
    dpi='';
    return;
end

dpi = dpiMenu();

if isempty(dpi)
    return;
end

    function format = formatMenu()

        keySet2 =   {'1','2','3'};
        valueSet2 = {'png','eps', 'pdf'};
        formatMap = containers.Map(keySet2,valueSet2);

        choice_format = helper_print_menu(formatMap, 'figure export format');

        if strcmp(choice_format,'e')
            format='';
            return;
        elseif isempty(choice_format)
            format = formatMap('1');
        elseif isKey(formatMap, choice_format)
            format = formatMap(choice_format);
        else
            warning(' Invalid input. Try again.');
            format = formatMenu();
        end
    end

    function dpi = dpiMenu()

        keySet3 =   {'1','2'};
        valueSet3 = {'300','manual input'};  
        dpiMap = containers.Map(keySet3,valueSet3);

        choice_dpi = helper_print_menu(dpiMap, 'figure export DPI');

        if strcmp(choice_dpi,'e')
            dpi = [];
            return;
        elseif isempty(choice_dpi)
            dpi = str2double(dpiMap('1'));
        elseif strcmp(choice_dpi,'1')
            dpi = str2double(dpiMap(choice_dpi));
        elseif strcmp(choice_dpi,'2')
            
            dpi_str = input(' provide DPI in integer: ','s'); % capture input as string
            
            man_dpi = str2double(dpi_str);
            
            if isreal(man_dpi) && rem(man_dpi,1)==0
                dpi = man_dpi;
            else
                warning(' Input is not an integer. Try again.');
                dpi = dpiMenu();
            end
            
        else
            warning(' Invalid input. Try again.');
            dpi = dpiMenu();

        end
    end

end

function [kpoints_no, write_mode] = kpointsMenu()

kpoints_no =  get_kpoints_no();

if isempty(kpoints_no)
    write_mode='';
    return;
end

write_mode = get_write_mode();

if isempty(write_mode)
    return;
end

    function kp_no = get_kpoints_no()
        
        keySet1 =   {'1','2'};
        valueSet1 = {'31','manual input'};  
        kpMap = containers.Map(keySet1,valueSet1);

        choice = helper_print_menu(kpMap, 'number of kpoints');

        if strcmp(choice,'e')
            kp_no = [];
            return;
        elseif isempty(choice) || strcmp(choice,'1')
            kp_no = str2double(kpMap('1'));
        elseif strcmp(choice,'2')

            dpi_str = input(' provide number of kpoints in integer: ','s'); % capture input as string

            man_dpi = str2double(dpi_str);

            if isreal(man_dpi) && rem(man_dpi,1)==0
                kp_no = man_dpi;
            else
                warning(' Input is not an integer. Try again.');
                kp_no = get_kpoints_no();
            end

        else
            warning(' Invalid input. Try again.');
            kp_no = get_kpoints_no();

        end

    end

    function write_mode = get_write_mode()
        
        keySet2 =   {'1','2'};
        valueSet2 = {'KPOINTS file','mat file'};  
        kpwMap = containers.Map(keySet2,valueSet2);

        choice = helper_print_menu(kpwMap, 'number of kpoints');

        if strcmp(choice,'e')
            write_mode = [];
            return;
        elseif isempty(choice)
            write_mode = kpwMap('1');
        elseif isKey(kpwMap, choice)
            write_mode = kpwMap(choice);
        else
            warning(' Invalid input. Try again.');
            write_mode = get_write_mode();

        end        
    end
        
end

function [cut_th, order, split_th] = mstarCalcMenu()

cut_th =  GetThreshold('dispersion cut-off energy', false);

if isempty(cut_th)
    order='';
    split_th=[];
    return;
end

order = GetOrder();

if isempty(order)
    split_th=[];
    return;
end

split_th =  GetThreshold('SOC splitting cut-off energy', true);

if isempty(split_th)
    return;
end

    function energy = GetThreshold(menu_str, split_flag)
        
        if ~split_flag
        keySet =   {'1','2'};
        valueSet = {'60meV','manual input'};
        else
            keySet =   {'1','2'};
            valueSet = {'5meV','manual input'};
        end
        energyMap = containers.Map(keySet,valueSet);
        choice_energy = helper_print_menu(energyMap, menu_str);

        if strcmp(choice_energy,'e')
            energy = [];
            return;
        elseif isempty(choice_energy)
            e_str = energyMap('1');
            energy = str2double(e_str(1:end-3));
        elseif strcmp(choice_energy,'1')
            e_str = energyMap('1');
            energy = str2double(e_str(1:end-3));
        elseif strcmp(choice_energy,'2')
            
            energy_inp_str = input(' provide energy (in meV): ','s'); % capture input as string
            
            try
                energy = str2double(energy_inp_str);
            catch
                warning(' cannot conver input to double. Try again');
                energy = GetThreshold(menu_str);
            end

        else
            warning(' Invalid input. Try again.');
            energy = GetThreshold(menu_str);

        end
        
    end

    function order = GetOrder()
        
        keySet1 =   {'1','2'};
        valueSet1 = {'[2 4 6]','[2 4 6 8]'};  
        orderMap = containers.Map(keySet1,valueSet1);
        choice_energy = helper_print_menu(orderMap,'order of effective mass fitting');

        if strcmp(choice_energy,'e')
            order = [];
            return;
        elseif isempty(choice_energy)
            order = str2num(orderMap('1'));
        elseif isKey(orderMap, choice_energy) && strcmp(choice_energy,'1')
            energy = str2num(orderMap(choice_energy));
        else
            warning(' Invalid input. Try again.');
            order = GetOrder();

        end
        
    end

end

function symbol = MatSym(mat_file)

all_data_mat_path = fullfile(cd,mat_file);
load(all_data_mat_path, 'mat');

for ii=1:length(mat.mat_no)
    keySet{ii}=num2str(ii);
    valueSet{ii}=mat.mat_sym(ii,:);
end

matMap = containers.Map(keySet,valueSet);

choice = helper_print_menu(matMap, 'band from material');

if strcmp(choice,'e')
    symbol = '';
    return;
elseif isempty(choice)
    symbol = matMap('1');
elseif isKey(kpwMap, choice)
    symbol = matMap(choice);
else
    warning(' Invalid input. Try again.');
    symbol = MatSym();

end

end

function [Emin, Emax] = EnergyRangeMenu()

Emin = -abs(GetEnergyValue('energy in negative y-axis'));

if isempty(Emin)
    Emax='';
    return;
end

Emax = GetEnergyValue('energy in positive y-axis');

if isempty(Emax)
    return;
end

    function energy = GetEnergyValue(energy_str)
        
        keySet =   {'1','2'};
        valueSet = {'2eV','manual input'};  
        energyMap = containers.Map(keySet,valueSet);
        choice_energy = helper_print_menu(energyMap, energy_str);

        if strcmp(choice_energy,'e')
            energy = [];
            return;
        elseif isempty(choice_energy)
            e_str = energyMap('1');
            energy = str2double(e_str(1:end-2));
        elseif strcmp(choice_energy,'1')
            e_str = energyMap('1');
            energy = str2double(e_str(1:end-2));
        elseif strcmp(choice_energy,'2')
            
            energy_inp_str = input(' provide energy (in eV): ','s'); % capture input as string
            
            try
                energy = str2double(energy_inp_str);
            catch
                warning(' cannot conver input to double. Try again');
                energy = GetEnergyValue(energy_str);
            end

        else
            warning(' Invalid input. Try again.');
            energy = GetEnergyValue(energy_str);

        end
        
    end

end

function dos_max = DOSMaxMenu()

keySet =   {'1','2'};
valueSet = {'15','manual input'};  
dosmaxMap = containers.Map(keySet,valueSet);

choice_dosmax = helper_print_menu(dosmaxMap, 'max number of states along x-axis');

if strcmp(choice_dosmax,'e')
    dos_max = [];
    return;
elseif isempty(choice_dosmax)
    dos_max = str2double(dosmaxMap('1'));
elseif strcmp(choice_dosmax,'1')
    dos_max = str2double(dosmaxMap(choice_dosmax));
elseif strcmp(choice_dosmax,'2')

    dosmax_str = input(' provide DPI in integer: ','s'); % capture input as string

    try
        dos_max = str2double(dosmax_str);
    catch
        warning(' cannot conver input to double. Try again');
        dos_max = DOSMaxMenu();
    end

else
    warning(' Invalid input. Try again.');
    dos_max = DOSMaxMenu();

end

end

function ProcessFile(mat_file)

% mat_file='all_data.mat';

xml_file = GetFileName();
FORCE_READ = GetForceFlag;

try
    python_or_matlab(xml_file, mat_file, FORCE_READ);
catch
    warning(' cannot read XML file. Try again');
    ProcessFile()
end

    function xml_file = GetFileName()

        keySet =   {'1','2'};
        valueSet = {'vasprun.xml','manual input'};  
        xmlMap = containers.Map(keySet,valueSet);

        choice_xml_file = helper_print_menu(xmlMap, 'XML file name');

        if strcmp(choice_xml_file,'e')
            return;
        elseif isempty(choice_xml_file)
            xml_file = xmlMap('1');
        elseif strcmp(choice_xml_file,'1')
            xml_file = xmlMap('1');
        elseif strcmp(choice_xml_file,'2')
            xml_file = input(' provide XML file name: ','s'); % capture input as string
        else
            warning(' Invalid input. Try again.');
            ProcessFile()
        end
    end

    function FORCE_READ = GetForceFlag()
        
        keySet =   {'1','2'};
        valueSet = {'no','yes'};  
        forceMap = containers.Map(keySet,valueSet);

        choice_force = helper_print_menu(forceMap, 'force read flag');

        if strcmp(choice_force,'e')
            return;
        elseif isempty(choice_force)
            FORCE_READ = false;
        elseif strcmp(choice_force,'1')
            FORCE_READ = false;
        elseif strcmp(choice_force,'2')
            FORCE_READ = true;
        else
            warning(' Invalid input. Try again.');
            FORCE_READ = GetForceFlag();
        end
    end

end


