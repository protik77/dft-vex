function print_bandgap(all_data_mat_file)

all_data_mat_path = fullfile(cd,all_data_mat_file);
load(all_data_mat_path, 'eigenvalue', 'Ef');

eigenvalue_Ef = eigenvalue - Ef;
[bg, ~, ~] = get_bandgap(eigenvalue_Ef);


if min(bg) > 0
    
    if length(bg)==1
        fprintf('\n Bandgap is %2.5f eV.', bg);
    elseif length(bg)==2
        if min(bg) > 0.5
            fprintf('\n Spin  up bandgap is %2.2f eV.', bg(1));            
            fprintf('\n Spin down bandgap is %2.2f eV.', bg(2));
        else
            fprintf('\n Spin  up bandgap is %3.1f meV.', bg(1)*1000);
            fprintf('\n Spin down bandgap is %3.1f meV.', bg(2)*1000);            
        end
    end
else
    fprintf('\n No band gap.');
    
end

fprintf('\n\n');

end