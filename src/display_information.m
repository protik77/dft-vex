function display_information(mat, dir_basis, rec_basis)

fprintf('\n')

fprintf('The System have:');
for ii=1:length(mat.mat_no)
    fprintf('\n\t\t\t%d atoms of %s', mat.mat_no(ii), mat.mat_sym(ii,:));
    ATOM_GROUP(ii)=mat.mat_no(ii);
    ATOM_TAG{ii}=mat.mat_sym(ii,:);
end
fprintf('.\n');

fprintf('\nDirect basis vectors are (without 2 * pi) :');
for ii=1:size(dir_basis,2)
    fprintf('\n\t\t\t\t\t%f\t%f\t%f', dir_basis(ii,1), dir_basis(ii,2), dir_basis(ii,3));
end
fprintf('\n');

fprintf('\nReciprocal basis vectors are (vec * 2 * pi) :');
for ii=1:size(rec_basis,2)
    fprintf('\n\t\t\t\t\t%f\t%f\t%f', rec_basis(ii,1), rec_basis(ii,2), rec_basis(ii,3));
end
fprintf('\n');

fprintf('\n')
end