function eigenvalue = read_eigenval(filename, eigenvalue_mat)

eigenval_size = size(eigenvalue_mat);
spin_no = eigenval_size(1);
eigenval_fullpath = fullfile(cd, filename);
bands_no=eigenval_size(2);
eigenvalue = zeros(eigenval_size);

if (exist(eigenval_fullpath,'file')==2)
    
    fid=fopen(eigenval_fullpath,'r');
    kp_ind=1;
    
    for il=1:7
        fgetl(fid);
    end

    while ~feof(fid)
        
        k_array = textscan(fid,'%f %f %f %f',1);
        
        if spin_no==1
            
            E_array = textscan(fid,'%d %f %f',bands_no);
            
            eigenvalue(1,:,kp_ind)=E_array{1, 2};
            
        elseif spin_no==2
            
        
            E_array = textscan(fid,'%d %f %f %f %f',bands_no);

            eigenvalue(1,:,kp_ind)=E_array{1, 2};
            eigenvalue(2,:,kp_ind)=E_array{1, 3};
        
        end
        
        fgetl(fid);
        kp_ind = kp_ind+1;

    end 
    
    fclose(fid);
    
else
    error('%s file does not exist.', filename)
end


end