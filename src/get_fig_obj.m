function fig = get_fig_obj(fig, fig_prop, fig_flag)

% fig=figure;
hold on;

Fontname='Arial';
Fontsize=14;
Linewidth=2;

set(gca, 'FontName', Fontname);
set(gca, 'FontSize', Fontsize);

switch fig_flag
    
    case 'bs'   % non spin polarized bandstructure
        yl=ylabel('Energy (eV)');
        set(gca, 'XTickLabel','');
        set(yl,'Fontsize',Fontsize);
        set(yl,'Fontname',Fontname);
        handler=gca;
        handler.LineWidth=Linewidth;
        handler.Box='on';
        handler.XTick=[];
        handler.YMinorTick='on';
        
    case 'bs-sp'    % spin polarized bandstructure
        yl=ylabel('Energy (eV)');
        set(gca, 'XTickLabel','');
        set(yl,'Fontsize',Fontsize);
        set(yl,'Fontname',Fontname);
        handler=gca;
        handler.LineWidth=Linewidth;
        handler.Box='on';
        handler.YMinorTick='on'; 
        
    case 'dos'    % spin polarized bandstructure
        yl=xlabel('DOS/states/eV');
        xl=ylabel('Energy (eV)');
        set(yl,'Fontsize',Fontsize);
        set(yl,'Fontname',Fontname);
        set(xl,'Fontsize',Fontsize);
        set(xl,'Fontname',Fontname);        
        handler=gca;
        handler.LineWidth=Linewidth;
        handler.Box='on';
        handler.YMinorTick='on';   
        
    case 'dos-origin'    % spin polarized bandstructure
        yl=xlabel('DOS/states/eV');
        xl=ylabel('Energy (eV)');
        set(yl,'Fontsize',Fontsize);
        set(yl,'Fontname',Fontname);
        set(xl,'Fontsize',Fontsize);
        set(xl,'Fontname',Fontname);        
        handler=gca;
        handler.Box='on';
        handler.LineWidth=Linewidth;
        handler.XAxis.LineWidth=0.1;
        handler.YMinorTick='on'; 
        handler.YGrid = 'on';
%         handler.YMinorGrid = 'on';
        
    case 'phonon'    % spin polarized bandstructure
        yl=ylabel('Frequency (THz)');
        xl=xlabel('q (A^{-1})');
        set(yl,'Fontsize',Fontsize);
        set(yl,'Fontname',Fontname);
        set(xl,'Fontsize',Fontsize);
        set(xl,'Fontname',Fontname);        
        handler=gca;
        handler.LineWidth=Linewidth;
        handler.Box='on';
        handler.YMinorTick='on';      
        
    case 'phonon-2d'    % spin polarized bandstructure
        xl=xlabel('Frequency [THz]');
        yl=ylabel('Velocity [ms^{-1}]');
        set(yl,'Fontsize',Fontsize);
        set(yl,'Fontname',Fontname);
        set(xl,'Fontsize',Fontsize);
        set(xl,'Fontname',Fontname);        
        handler=gca;
        handler.LineWidth=Linewidth;
        handler.Box='on';
        handler.YMinorTick='on';  
        
    case 'mstar'   % non spin polarized bandstructure
%         yl=ylabel('Energy (eV)');
%         set(yl,'Fontsize',fig_prop.Fontsize-2);
%         set(yl,'Fontname',fig_prop.Fontname);
        handler=gca;
        handler.LineWidth=Linewidth;
        handler.Box='on';
        handler.YMinorTick='on';        
        
end

end