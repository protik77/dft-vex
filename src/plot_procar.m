function plot_procar(all_data_mat_file, procar_prop, fig_prop)

all_data_mat_path = fullfile(cd,all_data_mat_file);
load(all_data_mat_path, 'kpoints', 'procar_data', 'eigenvalue', 'Ef',...
    'mat', 'kpoints_wt', 'incar_tags');

eigenvalue = eigenvalue-Ef;

[kpoints, eigenvalue, procar_data]=check_if_hse(true, incar_tags,...
    kpoints, kpoints_wt, eigenvalue, procar_data);

nokpoints = size(kpoints,1);
bands=size(eigenvalue,2);
ions_no = size(procar_data,4);
spin_no = size(procar_data,3);
[from_band, to_band]=get_min_max_band(eigenvalue,procar_prop);
[k_path, HSPdata] = get_Kpath(kpoints, true);
fform=sprintf('-d%s', fig_prop.format);
fdpi=sprintf('-r%d', fig_prop.dpi);
ionic_legend={'s','p','d'};

colors               = {
                        [ 0.16,     0.44,    1.00 ],...
                        [ 0.93,     0.00,    0.00 ],...
                        [ 0.00,     0.57,    0.00 ],...
                        [ 0.17,     0.17,    0.17 ],...
                        [ 0.44,     0.00,    0.99 ],...
                        [ 1.00,     0.50,    0.10 ],...
                        [ 0.75,     0.00,    0.75 ],...
                        [ 0.50,     0.50,    0.50 ],...
                        [ 0.50,     0.57,    0.00 ],...
                        [ 0.00,     0.00,    0.00 ]
                       };


for ii=1:length(mat.mat_no)
    ATOM_GROUP(ii)=mat.mat_no(ii);
    ATOM_TAG{ii}=mat.mat_sym(ii,:);
end

a=1;
for ig=1:size(ATOM_GROUP,2)    
    gv=ATOM_GROUP(ig);    
    for i=1:gv        
        ATOM_seq{a}=ATOM_TAG{ig};
        a=a+1;
    end    
end
clear a ig gv;
% 
% this should work for both spin and no-spin plarized cases
if spin_no==2
    sp_spin=2;
else
    sp_spin=1;
end

procar_data_file_name='procar_data.mat';
procar_data_file_path = fullfile(cd, procar_data_file_name);

READ_FLAG = readOrNOT(all_data_mat_file, procar_data_file_name);

if (exist(procar_data_file_path,'file')==2)
    MATVariables = who('-file', procar_data_file_path);
else
    MATVariables={};
end

% if the all_data.mat file is newer than procar_data.mat file and
% the procar_data.mat file does not have plane_sum variable,
% then sum them.
% Else, load from procar_data.mat file.
if READ_FLAG && ~ismember('plane_sum', MATVariables)
    [plane_sum, grand_sum, ion_data, orbital_data, Total_Orbital] ...
        = procar_sum('all_data.mat', procar_data_file_name);
else
    fprintf(' Reading from procar data file (%s).\n\n', procar_data_file_name)
    load(procar_data_file_path);
end


switch procar_prop.procar_flag
    
    case 'ionic' 
    % plotting

    if size(orbital_data,1)==1

        figure('Units','inches','position',[1 1 6 4]);
        get_fig_obj(gca, fig_prop, 'bs');
        axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
        
        for i = 1:length(ATOM_GROUP)
            plot(-100:-100,i,'o','MarkerEdgeColor',colors{i},'MarkerFaceColor',colors{i},'MarkerSize',5);
        end     
        
        % define color matrix for ion types
        atom_type=size(ATOM_TAG,2);
        cmatrix=nan*ones(nokpoints, bands); % color matrix for atomic procar        
        for b = from_band:to_band
            for k = 1:nokpoints                           
                val = max(ion_data(1,k,b,:));
                for i = 1:ions_no % for each ion
                    if ion_data(1,k,b,i) >= val

                        for it=1:atom_type
                            if ( strcmp(ATOM_seq{i}, ATOM_TAG{it}))
                                cmatrix(k,b)=it;
                            end
                        end
                       break;
                    end
                end
            end
        end
        
        kk=[k_path;k_path];
        
        for b=from_band:to_band
            this_eig=squeeze(eigenvalue(1,b,:))';
            bb=[this_eig;this_eig];
            col=cmatrix(:,b)';
            cc=[col;col];
            surface(kk, bb, cc, 'facecol','no', 'edgecol','interp','linew',fig_prop.Linewidth);
        end

        add_xtick(HSPdata, k_path, procar_prop, fig_prop);
        
        % define colormap
        lgnd_flag=false(atom_type,1);
        ind=1;
        for ig=1:atom_type
            if sum(sum(squeeze(cmatrix(:,:))==ig)) ~= 0        
                plot(-100:-100,ig,'o','MarkerEdgeColor',colors{ind},...
                    'MarkerFaceColor',colors{ind},'MarkerSize',5);
                cust_map(ind,:)=colors{ind};
                ind=ind+1;
                lgnd_flag(ig)=true;
            end
            
        end
        
        colormap(cust_map);
        % sometimes the legend plots extra elements.
        % it is due to the face the the bands are segregated as a whole
        % so even though the element is not existent in the figure,
        % the remaining energies of the same band have these elements
        legend(ATOM_TAG(lgnd_flag), 'Location', 'best');        

    else

    for spin=1:size(eigenvalue,1)
        if spin==1
            figure('Units','inches','position',[1 1 12 8]);
            fig1=subplot(1,2,spin);
            get_fig_obj(gca, fig_prop, 'bs');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('Spin up');
        else
            fig2 = subplot(1,2,spin);
            linkaxes([fig1,fig2],'xy');
            get_fig_obj(gca, fig_prop, 'bs');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('Spin down');
        end
        
        for i = 1:length(ATOM_GROUP)
            plot(-100:-100,i,'o','MarkerEdgeColor',colors{i},'MarkerFaceColor',colors{i},'MarkerSize',5);
        end
        
        % define color matrix for ion types
        atom_type=size(ATOM_TAG,2);
        cmatrix=nan*ones(nokpoints, bands); % color matrix for atomic procar        
        for b = from_band:to_band
            for k = 1:nokpoints                           
                val = max(ion_data(spin,k,b,:));
                for i = 1:ions_no % for each ion
                    if ion_data(spin,k,b,i) >= val

                        for it=1:atom_type
                            if ( strcmp(ATOM_seq{i}, ATOM_TAG{it}))

                                cmatrix(k,b)=it;
                            end
                        end
                       break;
                    end
                end
            end
        end
        
        
        % define colormap
        lgnd_flag=false(atom_type,1);
        ind=1;
        for ig=1:atom_type
            if sum(sum(squeeze(cmatrix(spin,:,:))==ig)) ~= 0      
                plot(-100:-100,ig,'o','MarkerEdgeColor',colors{ig},...
                    'MarkerFaceColor',colors{ig},'MarkerSize',5);
                cust_map(ind,:)=colors{ig};
                ind=ind+1;
                lgnd_flag(ig)=true;
            end
            
        end
        
        kk=[k_path;k_path];
        
        for b=from_band:to_band
            this_eig=squeeze(eigenvalue(spin,b,:))';
            bb=[this_eig;this_eig];
            col=cmatrix(:,b)';
            cc=[col;col];
            surface(kk, bb, cc, 'facecol','no', 'edgecol','interp','linew',fig_prop.Linewidth)
            
        end
        
        colormap(cust_map);
        
        add_xtick(HSPdata, k_path, procar_prop, fig_prop);
        
        if spin==2
        	legend(ATOM_TAG(lgnd_flag), 'Location', 'best'); 
        end

    end
    end
    
    print('ionic_composition',fform, fdpi);
    logstr=sprintf('Ionic contribution from PROCAR is plotted in %s format with %d DPI.', fig_prop.format, fig_prop.dpi);
    WriteLog(logstr);     
    
    case 'orbital' 
    
    if size(orbital_data,1)==1
        
        figure('Units','inches','position',[1 1 6 4]);
        get_fig_obj(gca, fig_prop, 'bs');
        axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
        
        for i = 1:3 % 3 for s, p and d orbital
            plot(-100:-100,i,'o','MarkerEdgeColor',colors{i},...
                'MarkerFaceColor',colors{i},'MarkerSize',5);
        end
        
        cmatrix=nan*ones(sp_spin, nokpoints, bands); % color matrix for atomic procar        
        
        for b = from_band:to_band
            for k = 1:nokpoints                           
                [~,i] = max(Total_Orbital(sp_spin,k,b,:));
                cmatrix(sp_spin,k,b)=i;
            end
        end
        
        orbital_type=3; % s, p and d oribtal
        
        ind=1;
        % define colormap
        for ig=1:orbital_type
            if sum(sum(squeeze(cmatrix(sp_spin,:,:))==ig))==0
                lgnd_flag(ig)=false;
            else          
                plot(-100:-100,ig,'o','MarkerEdgeColor',colors{ind},...
                    'MarkerFaceColor',colors{ind},'MarkerSize',5);
                cust_map(ind,:)=colors{ind};
                ind=ind+1;
                lgnd_flag(ig)=true;
            end
            
        end
        
        legend(ionic_legend(lgnd_flag), 'Location', 'best', 'AutoUpdate','off'); 
        
        kk=[k_path;k_path];
        
        for b=from_band:to_band
            this_eig=squeeze(eigenvalue(sp_spin,b,:))';
            bb=[this_eig;this_eig];
            col=squeeze(cmatrix(sp_spin,:,b));
            cc=[col;col];
            surface(kk, bb, cc, 'facecol','no', 'edgecol','interp','linew',fig_prop.Linewidth)
            
        end
        
        colormap(cust_map);

        add_xtick(HSPdata, k_path, procar_prop, fig_prop);

    else  
        
    for spin=1:size(eigenvalue,1)
        if spin==1
            figure('Units','inches','position',[1 1 12 8]);
            fig1=subplot(1,2,spin);
            get_fig_obj(gca, fig_prop, 'bs');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('Spin up');
        else
            fig2=subplot(1,2,spin);
            linkaxes([fig1,fig2],'xy');
            get_fig_obj(gca, fig_prop, 'bs');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('Spin down');
        end  
        
        % define color matrix for ion types
        orbital_type=3; % s, p and d oribtal
        if spin==1
        cmatrix=nan*ones(sp_spin, nokpoints, bands); % color matrix for atomic procar        
        end
        
        for b = from_band:to_band
            for k = 1:nokpoints                           
                [~,i] = max(squeeze(Total_Orbital(spin,k,b,:)));
                cmatrix(spin,k,b)=i;
            end
        end
        
        ind=1;
        % define colormap
        for ig=1:orbital_type
            if sum(sum(squeeze(cmatrix(spin,:,:))==ig))==0
                lgnd_flag(ig)=false;
            else          
                plot(-100:-100,ig,'o','MarkerEdgeColor',colors{ig},...
                    'MarkerFaceColor',colors{ig},'MarkerSize',5);
                cust_map(ind,:)=colors{ig};
                ind=ind+1;
                lgnd_flag(ig)=true;
            end
            
        end
        
        if spin==2
        legend(ionic_legend(lgnd_flag), 'Location', 'best'); 
        end

        
        kk=[k_path;k_path];
        
        for b=from_band:to_band
            this_eig=squeeze(eigenvalue(spin,b,:))';
            bb=[this_eig;this_eig];
            col=squeeze(cmatrix(spin,:,b));
            cc=[col;col];
            surface(kk, bb, cc, 'facecol','none', 'edgecol','interp','linew',fig_prop.Linewidth)
            
        end
        
        colormap(cust_map);
        
        add_xtick(HSPdata, k_path, procar_prop, fig_prop);
    
    end
    end
    
    print('orbital_composition',fform, fdpi,'-painters');
    logstr=sprintf('orbital contribution from PROCAR is plotted in %s format with %d DPI.', fig_prop.format, fig_prop.dpi);
    WriteLog(logstr);
    
    case 's-contrib'
        
    if size(orbital_data,1)==1
        
%         figure('Units','normalized','position',[.1 .1 .3 .4])
        figure('Units','inches','position',[1 1 6 4]);
        get_fig_obj(gca, fig_prop, 'bs');
        axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
        title('s-orbital');
        
        kk=[k_path;k_path];
        
        for b=from_band:to_band
            this_eig=squeeze(eigenvalue(sp_spin,b,:))';
            bb=[this_eig;this_eig];
            frac=squeeze(plane_sum(sp_spin,1,1,b,:))'*100./squeeze(grand_sum(sp_spin,:,b));
            cc=[frac;frac];

            surface(kk, bb, cc, 'facecol','no', 'edgecol','interp','linew', fig_prop.Linewidth)

        end
        
        caxis manual % set colorbar manual
        caxis([0 100]); % set the values 0 t0 100%
        colormap(flipud(autumn));    
        
        add_xtick(HSPdata, k_path, procar_prop, fig_prop);  
        
        h = colorbar;
        ylabel(h,'% Contribution');
        
    else
        for spin=1:2
            
        if spin==1
%             figure('Units','normalized','position',[.1 .1 .6 .4])
            figure('Units','inches','position',[1 1 12 4.2]);
            fig1=subplot(1,2,spin);
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('s-orbital (up-spin)');
        else
            fig2=subplot(1,2,spin);
            linkaxes([fig1,fig2],'xy');
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('s-orbital (down-spin)');
        end  
        
        kk=[k_path;k_path];
        
        for b=from_band:to_band
            this_eig=squeeze(eigenvalue(sp_spin,b,:))';
            bb=[this_eig;this_eig];
            frac=squeeze(plane_sum(spin,1,1,b,:))'*100./squeeze(grand_sum(spin,:,b));
            cc=[frac;frac];

            surface(kk, bb, cc, 'facecol','no', 'edgecol','interp','linew', fig_prop.Linewidth)

        end
        
        caxis manual % set colorbar manual
        caxis([0 100]); % set the values 0 t0 100%
        colormap(flipud(autumn));

        add_xtick(HSPdata, k_path, procar_prop, fig_prop);
        
        end
        
        hp4 = get(subplot(1,2,2),'Position');
        h = colorbar('Position',[hp4(1)+hp4(3)+0.015  hp4(2)  0.03  hp4(2)+hp4(3)*2.1]);
        ylabel(h,'% Contribution');
        
    end
    
    print('contribution_s_orbital',fform, fdpi,'-painters');
    logstr=sprintf('contribution of s-orbital from PROCAR is plotted in %s format with %d DPI.', fig_prop.format, fig_prop.dpi);
    WriteLog(logstr);
    
    case 'sigma-pi'
        
    if size(orbital_data,1)==1
        
        for plane=1:2
            
        if plane==1
            figure('Units','normalized','position',[.1 .1 .6 .4])
%             figure('Units','inches','position',[1 1 12 8]);
            fig1=subplot(1,2,plane);
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('p-orbital \sigma');
        else
            fig2=subplot(1,2,plane);
            linkaxes([fig1,fig2],'xy');
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('p-orbital \pi');
        end  
        
        kk=[k_path;k_path];
        
        for b=from_band:to_band
            this_eig=squeeze(eigenvalue(sp_spin,b,:))';
            bb=[this_eig;this_eig];
            frac=squeeze(plane_sum(sp_spin,2,plane,b,:))'*100./squeeze(grand_sum(sp_spin,:,b));
            cc=[frac;frac];

            surface(kk, bb, cc, 'facecol','no', 'edgecol','interp','linew', fig_prop.Linewidth)

        end
        
        caxis manual % set colorbar manual
        caxis([0 100]); % set the values 0 t0 100%
        colormap(flipud(autumn)); 
        
        add_xtick(HSPdata, k_path, procar_prop, fig_prop);
        
        end
        
        hp4 = get(subplot(1,2,2),'Position');
        h = colorbar('Position',[hp4(1)+hp4(3)+0.015  hp4(2)  0.03  hp4(2)+hp4(3)*2.1]);
        ylabel(h,'% Contribution');
        
    else
        
        for spin=1:sp_spin
        for plane=1:2
            
        if spin==1 && plane==1
%             figure('Units','normalized','position',[.1 .1 .6 .4])
            figure('Units','inches','position',[1 1 12 8]);
            fig1=subplot(2,2,1);
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('p-orbital \sigma (up-spin)');
        elseif spin==1 && plane==2
            fig2=subplot(2,2,2);
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('p-orbital \pi  (up-spin)');
        elseif spin==2 && plane==1
            fig3=subplot(2,2,3);
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('p-orbital \sigma  (down-spin)');
        elseif spin==2 && plane==2
            fig4=subplot(2,2,4);
            linkaxes([fig1,fig2, fig3, fig4],'xy');
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('p-orbital \pi  (down-spin)');
        end  
        
        kk=[k_path;k_path];
        
        for b=from_band:to_band
            this_eig=squeeze(eigenvalue(sp_spin,b,:))';
            bb=[this_eig;this_eig];
            frac=squeeze(plane_sum(sp_spin,3,spin,b,:))'*100./squeeze(grand_sum(sp_spin,:,b));
            cc=[frac;frac];

            surface(kk, bb, cc, 'facecol','no', 'edgecol','interp','linew', fig_prop.Linewidth)

        end
        
        caxis manual % set colorbar manual
        caxis([0 100]); % set the values 0 t0 100%
        colormap(flipud(autumn)); 
        
        add_xtick(HSPdata, k_path, procar_prop, fig_prop);
        
        end
        end
        
        hp4 = get(subplot(2,2,4),'Position');
        h = colorbar('Position',[hp4(1)+hp4(3)+0.015  hp4(4)/1.25  0.03  (hp4(2)+hp4(3))]);
        ylabel(h,'% Contribution');        

    end
    
    print('contribution_p_orbital',fform, fdpi,'-painters');
    logstr=sprintf('contribution of p-orbital from PROCAR is plotted in %s format with %d DPI.', fig_prop.format, fig_prop.dpi);
    WriteLog(logstr);
    
    case 't2g-eg'
        
    if size(orbital_data,1)==1
        
        for plane=1:2
            
        if plane==1
%             figure('Units','normalized','position',[.1 .1 .6 .4])
            figure('Units','inches','position',[1 1 12 4]);
            fig1=subplot(1,2,plane);
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('d-orbital e_g');
        else
            fig2=subplot(1,2,plane);
            linkaxes([fig1,fig2],'xy');
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('d-orbital t_{2g}');
        end  
        
        kk=[k_path;k_path];
        
        for b=from_band:to_band
            this_eig=squeeze(eigenvalue(sp_spin,b,:))';
            bb=[this_eig;this_eig];
            frac=squeeze(plane_sum(sp_spin,3,plane,b,:))'*100./squeeze(grand_sum(sp_spin,:,b));
            cc=[frac;frac];

            surface(kk, bb, cc, 'facecol','no', 'edgecol','interp','linew', fig_prop.Linewidth)

        end
        
        add_xtick(HSPdata, k_path, procar_prop, fig_prop);
        
        caxis manual % set colorbar manual
        caxis([0 100]); % set the values 0 t0 100%
        colormap(flipud(autumn));
        
        end
        
        hp4 = get(subplot(1,2,2),'Position');
        h = colorbar('Position',[hp4(1)+hp4(3)+0.015  hp4(2)  0.03  hp4(2)+hp4(3)*2.03]);
        ylabel(h,'% Contribution');         
              
    else
        
        for spin=1:sp_spin
        for plane=1:2
            
        if spin==1 && plane==1
%             figure('Units','normalized','position',[.1 .1 .6 .4])
            figure('Units','inches','position',[1 1 12 8]);
            fig1=subplot(2,2,1);
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('d-orbital e_g (up-spin)');
        elseif spin==1 && plane==2
            fig2=subplot(2,2,2);
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('p-orbital t_{2g}  (up-spin)');
        elseif spin==2 && plane==1
            fig3=subplot(2,2,3);
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('d-orbital e_g  (down-spin)');
        elseif spin==2 && plane==2
            fig4=subplot(2,2,4);
            linkaxes([fig1,fig2, fig3, fig4],'xy');
            get_fig_obj(gca, fig_prop, 'bs-sp');
            axis([k_path(1) k_path(end) procar_prop.Emin procar_prop.Emax]);
            title('p-orbital t_{2g} (down-spin)');
        end  
        
        kk=[k_path;k_path];
        
        for b=from_band:to_band
            this_eig=squeeze(eigenvalue(sp_spin,b,:))';
            bb=[this_eig;this_eig];
            frac=squeeze(plane_sum(sp_spin,3,spin,b,:))'*100./squeeze(grand_sum(sp_spin,:,b));
            cc=[frac;frac];

            surface(kk, bb, cc, 'facecol','no', 'edgecol','interp','linew', fig_prop.Linewidth)

        end
        
        caxis manual % set colorbar manual
        caxis([0 100]); % set the values 0 t0 100%
        colormap(flipud(autumn));
        
        
        add_xtick(HSPdata, k_path, procar_prop, fig_prop);
  
        end
        end
        
        hp4 = get(subplot(2,2,4),'Position');
        h = colorbar('Position',[hp4(1)+hp4(3)+0.015  hp4(4)/1.25  0.03  (hp4(2)+hp4(3))]);
        ylabel(h,'% Contribution');        
        
    end
    
    print('contribution_d_orbital',fform, fdpi,'-painters');
    logstr=sprintf('contribution of d-orbital from PROCAR is plotted in %s format with %d DPI.', fig_prop.format, fig_prop.dpi);
    WriteLog(logstr);
end

end