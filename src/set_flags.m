function y = set_flags(incar_tags)

% initialize flags
y= struct( 'HSE_FLAG', false,...
    'DOS_FLAG', false,...
    'SOC_FLAG', false,...
    'EFIELD_FLAG', false...
    );

read_fields = fieldnames(incar_tags, '-full');

for ridx = 1: length(read_fields)

    % check for SOC flags
    if strcmp(read_fields{ridx}, 'LSORBIT') && strcmp(incar_tags.(read_fields{ridx}), 'T')
        y.SOC_FLAG = true;

    % check for HSE flags
    elseif strcmp(read_fields{ridx}, 'LHFCALC') && strcmp(incar_tags.(read_fields{ridx}), 'T')
        y.HSE_FLAG = true;  

    % check for ELECTRIC FIELD flags
    elseif strcmp(read_fields{ridx}, 'LDIPOL') && strcmp(incar_tags.(read_fields{ridx}), 'T')
        y.EFIELD_FLAG = true;
        warning('Direction and value for electric field is not implemented')
    end      

end

end