function saveProcarTestData()

readFromPython('no_sp_no_soc.xml', 0);

load('all_data.mat')

procar_prop.orbitals = cellstr(orbital_list);
procar_prop.mat_no=mat.mat_no;
procar_prop.mat_sym=mat.mat_sym;
procar_prop.kpoints=kpoints;

[ionic_data, orbital_data] = procar_sum(procar_prop, procar_data, eigenvalue);

save('procar_no_sp_no_soc.mat', 'ionic_data', 'orbital_data');

clearvars;

readFromPython('sp_no_soc.xml', 1);

load('all_data.mat')

procar_prop.orbitals = cellstr(orbital_list);
procar_prop.mat_no=mat.mat_no;
procar_prop.mat_sym=mat.mat_sym;
procar_prop.kpoints=kpoints;

[ionic_data, orbital_data] = procar_sum(procar_prop, procar_data, eigenvalue);

save('procar_sp_no_soc.mat', 'ionic_data', 'orbital_data');

clearvars;

readFromPython('sp_soc.xml', 0);

load('all_data.mat')

procar_prop.orbitals = cellstr(orbital_list);
procar_prop.mat_no=mat.mat_no;
procar_prop.mat_sym=mat.mat_sym;
procar_prop.kpoints=kpoints;

[ionic_data, orbital_data] = procar_sum(procar_prop, procar_data, eigenvalue);

save('procar_sp_soc.mat', 'ionic_data', 'orbital_data');

end