function [k_path, HSPdata] = get_Kpath(kpoints, print_info)

% calculating angle between two direct vectors

% dir_angle= acosd(dot(dir_basis(1,:),dir_basis(2,:))/(norm(dir_basis(1,:))*norm(dir_basis(2,:))));

% rec_angle= acosd(dot(rec_basis(1,:),rec_basis(2,:))/(norm(rec_basis(1,:))*norm(rec_basis(2,:))));

% if (abs(60-dir_angle)<=1E-3)
%     K_cord=[0.3333 0.6667 0.0];
%     fprintf('\nAngle between basis vectors is %3.0f degrees. \n', dir_angle);
%     fprintf('Coordinate of K-point is set as %0.4f %0.4f %0.4f.\n', K_cord(1),K_cord(2),K_cord(3));
% elseif(abs(120-dir_angle)<=1E-3)
%     K_cord=[0.3333 0.3333 0.0];
%     fprintf('\nAngle between basis vectors is %3.0f degrees. \n', dir_angle);
%     fprintf('Coordinate of K-point is set as %0.4f %0.4f %0.4f.\n', K_cord(1),K_cord(2),K_cord(3));
% else
%     fprintf('\nThe angle cannot be determined. An arbitrary value is set for K\n');
%     K_cord=[0.3333 0.3333 0.0];
% end

% labels for HSP path
% template: {[],''},
kzsdt=0.35;     % kz value for the so called S-D-T points (3D dirac fermion PtTe2 paper)
HSPs = {{[0 0 0],'\Gamma'},
    {[3/8 3/4 3/8],'K'},  
    {[0 0.5 0.5],'X'},      
    {[1/4 3/4 0.5],'W'},        
    {[1/4 5/8 5/8],'U'},    
%   [0.5 0.0 0.5],''}, % or sometimes this for L   
    {[0.5 0.0 kzsdt],'S'},
    {[0.0 0.0 kzsdt],'D'},
    {[0.3333 0.3333 kzsdt],'T'},
%     {[0.5 0.0 0.5],'X'},
    {[0.5 0.25 0.75],'W'},
    {[0.0 0.0 0.5],'Y'}, % for monoclinic NbS3
    {[0.0  0.439875    0.71049342],'H'},
%     {[0.0  0.5  0.5],'C'},
    {[0.5  0.5  0.5],'E'},
    {[0.5  0.560125    0.28950658],'M_1'},
    {[0.5  0.5  0.],'A'},
    {[0.0  0.5  0.],'X'},
    {[0.   0.560125    0.28950658],'H_1'},
    {[0.5  0.439875    0.71049342],'M'},
%     {[0.5  0.0  0.5],'D'},
    {[0.5  0.0  0.0],'Z'},
    {[0.0  0.0   0.5],'Y'},
    {[0.5 0.0 0.0],'M'},
    {[0.375 0.375 0.75],'K'},  
    {[0.625 0.25 0.625],'U'}, 
    {[0.0 0.5 0.0],'M'}, % M for square BZ
    {[0.5 0.5 0.0],'M'}, % M for square BZ
%     {[0.5 0.0 0.0],'X'}, % X for square BZ
    {[0.0 0.0 0.5],'A'},
    {[0.0 0.0 -0.5],'A'},
    {[0.5 0.5 0.5],'L'},  
    {[0.5  0.0  0.5],'L'},    
    {[0.5 0.5 0.5],'L'},         
    {[0.3333 0.3333 0.0],'K'},
    {[0.6666 0.3333 0.0],'K'''},    
    {[0.6667 -0.3333 0.0],'K'''},
    {[0.3333 0.6667 0.0],'K'},
    {[0.0 0.3333 0.3333],'K'},  
    {[0.3333 0.3333 0.5],'H'},    
    };

%     kpoints=kpoints;
%     nokpoints= kpoints_no;
%     KPOINTS=nokpoints;
    
%     clear kpoint_wts;

% direction increase flags
X_inc = false;
Y_inc = false;
Z_inc = false;


% Mark change of direction
if ( (kpoints(2,1)-kpoints(1,1)) > 0)
    X_inc = true;
end
if ( (kpoints(2,2)-kpoints(1,2)) > 0)
    Y_inc = true;
end
if ( (kpoints(2,3)-kpoints(1,3)) > 0)
    Z_inc = true;
end

% k_path is the variable for band structure plot
nokpoints = size(kpoints,1);
k_path = zeros(1,nokpoints);

% high symmetry points for band structure path
hsp_bs(1) = 1;
hsp_bs(2) = nokpoints;        % Last High symmetry points:    Gamma points
count = 3;

for k = 2:nokpoints
    
    k_path(1,k) = k_path(1,k-1) + pdist2(kpoints(k,:),kpoints(k-1,:));

    if ( X_inc  && (kpoints(k,1)-kpoints(k-1,1)) < 0)
        X_inc = false;

        hsp_bs(count) = k-1;
        count = count + 1;
    end
    if ( ~X_inc && (kpoints(k,1)-kpoints(k-1,1)) > 0)
        X_inc = true;
        hsp_bs(count) = k-1;
        count = count + 1;
    end

    if ( Y_inc && (kpoints(k,2)-kpoints(k-1,2)) < 0)
        Y_inc = false;
        hsp_bs(count) = k-1;
        count = count + 1;
    end
    if ( ~Y_inc && (kpoints(k,2)-kpoints(k-1,2)) > 0)
        Y_inc = true;
        hsp_bs(count) = k-1;
        count = count + 1;
    end

    if ( Z_inc && (kpoints(k,3)-kpoints(k-1,3)) < 0)
        Z_inc = false;
        hsp_bs(count) = k-1;
        count = count + 1;
    end
    if ( ~Z_inc && (kpoints(k,3)-kpoints(k-1,3)) > 0)
        Z_inc = true;
        hsp_bs(count) = k-1;
        count = count + 1;
    end

end

hsp_bs = sort(unique(hsp_bs));

% PARTIAL_FLAG = false;
HSPdata={};
for is=1:size(hsp_bs,2)
    found_flag=false;
    for id = 1: length(HSPs)
        
        if (abs(kpoints(hsp_bs(is),:)-HSPs{id}{1})<=1E-4)
            HSPdata{is}{1} = hsp_bs(is);
            HSPdata{is}{2} = HSPs{id}{2};
            found_flag=true;
%             break;
        end
    end
    
    if ~found_flag
        HSPdata{is}{1} = hsp_bs(is);
        HSPdata{is}{2} = '';
    end        
end

% % update 2
% % PARTIAL_FLAG = false;
% HSPdata={};
% for ik=1:size(kpoints,1)
%     found_flag=false;
%     for id = 1: length(HSPs)
%         
%         if (abs(kpoints(ik,:)-HSPs{id}{1})<=1E-4)
%             HSPdata{ik}{1} = kpoints(ik,:);
%             HSPdata{ik}{2} = HSPs{id}{2};
%             found_flag=true;
% %             break;
%         end
%     end
%     
%     if ~found_flag
%         HSPdata{ik}{1} = hsp_bs(is);
%         HSPdata{ik}{2} = '';
%     end        
% end

% if isempty(HSPdata)
%     PARTIAL_FLAG= true;
% end
    
if print_info
    fprintf('\n ==================================================\n');
    for ix=1:length(HSPdata)
        fprintf(' Symmetry point %2d: %6s (%6.4f, %6.4f, %6.4f)\n',...
            ix, HSPdata{ix}{2}, kpoints(HSPdata{ix}{1},1),...
            kpoints(HSPdata{ix}{1},2), kpoints(HSPdata{ix}{1},3));
    end
    fprintf(' ==================================================\n\n');
end
% 
% fprintf('\n')
end