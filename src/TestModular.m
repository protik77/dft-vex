function tests = TestModular
    tests = functiontests(localfunctions);
end

% function testSimple(testCase)
% 
% load('all_data.mat')
% 
% assert_y= struct( 'HSE_FLAG', false,...
%     'DOS_FLAG', false,...
%     'SOC_FLAG', false,...
%     'EFIELD_FLAG', false...
%     );
% 
% func_y = set_flags(incar_tags);
% verifyEqual(testCase, func_y, assert_y)
% end


function testProcarSimple(testCase)

readFromPython('no_sp_no_soc.xml', 0);

load('all_data.mat');

procar_prop.orbitals = cellstr(orbital_list);
procar_prop.mat_no=mat.mat_no;
procar_prop.mat_sym=mat.mat_sym;
procar_prop.kpoints=kpoints;

[ionic_data1, orbital_data1] = procar_sum(procar_prop, procar_data, eigenvalue);

load('procar_no_sp_no_soc.mat')

verifyEqual(testCase, ionic_data, ionic_data1);
verifyEqual(testCase, orbital_data, orbital_data1)
end

function testProcarSP(testCase)

readFromPython('sp_no_soc.xml', 1);

load('all_data.mat');

procar_prop.orbitals = cellstr(orbital_list);
procar_prop.mat_no=mat.mat_no;
procar_prop.mat_sym=mat.mat_sym;
procar_prop.kpoints=kpoints;

[ionic_data1, orbital_data1] = procar_sum(procar_prop, procar_data, eigenvalue);

load('procar_sp_no_soc.mat')

verifyEqual(testCase, ionic_data, ionic_data1);
verifyEqual(testCase, orbital_data, orbital_data1)
end

function testProcarSOC(testCase)

readFromPython('soc.xml', 0);

load('all_data.mat');

procar_prop.orbitals = cellstr(orbital_list);
procar_prop.mat_no=mat.mat_no;
procar_prop.mat_sym=mat.mat_sym;
procar_prop.kpoints=kpoints;

[ionic_data1, orbital_data1] = procar_sum(procar_prop, procar_data, eigenvalue);

load('procar_soc.mat')

verifyEqual(testCase, ionic_data, ionic_data1);
verifyEqual(testCase, orbital_data, orbital_data1)
end
