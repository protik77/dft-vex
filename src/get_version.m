function [version_no, version_year] = get_version
% This function returns version number and the more common version with
% year.
% The version_no variable is a double with x.y where x is the major version
% and y is the minor version.
% The version_year returns a struct with first element is the year e.g.
% 2016 and the second element is the a or b

[v,~] = version;

v2 = strsplit(v);

version_no = str2double(v2{1}(1:3));

version_year{1} = str2double(v2{2}(3:6));
version_year{2} = v2{2}(7);

end