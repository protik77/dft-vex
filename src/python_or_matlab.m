function python_or_matlab(xml_file, mat_file, FORCE_READ)

python_v = 2.7;

[version, ~, ~] = pyversion;

if str2double(version)> python_v %&& isloaded
    fprintf(' Python V%s found and loaded. Will use python XML reader if needed.\n', version);
    MATLAB_flag=false;
else
    fprintf(' Working python not found.\n Using Matlab''s xml reader. It might take some time.\n')
    MATLAB_flag=true;
end

if FORCE_READ
    READ_FLAG = true;
else
    READ_FLAG = readOrNOT(xml_file, mat_file);
end

% mat_fullpath = fullfile(cd, mat_file);
xml_fullpath = fullfile(cd, xml_file);

if READ_FLAG
    
    fprintf('\n')
    
    if MATLAB_flag
        XMLReadMatlab(xml_fullpath)
    else
        try
            [script_path,~,~]=fileparts(mfilename('fullpath'));
            readFromPython(script_path, xml_file);
        catch
            warning('Reading from python failed. Using Matlab XML reader.');
            XMLReadMatlab(xml_fullpath)
        end
            
    end
else
    fprintf(' Reading from mat file %s.\n', mat_file);
end

end