function readFromPython(script_path, xml_file)

% machine_name = getenv('computername');
% 
% if strcmp(machine_name, 'LAKE-LAB-001')
%     script_path = 'C:\Dropbox\Dropbox\RnD\PythonProjectsCodes\VaspPythonXML\PyCharmVasprunXML';
% elseif strcmp(machine_name, 'PROTIXPC')
%     script_path = 'D:\Users\Protik2\Dropbox\RnD\PythonProjectsCodes\VaspPythonXML\PyCharmVasprunXML';
% else
%     error('Unknown machine. Python reader location unknown.')
% end

python_xml_script='PythonVaspRunTest.py';

script_path_with_name = fullfile(script_path, python_xml_script);

if exist(script_path_with_name, 'file')==2
    fprintf(' Python XML reader script exists.\n\n');    
else
    msg = sprintf(' Python XML script does not exist in: %s.', script_path_with_name);
    error(msg);
    clear msg;
end

python_path = py.sys.path;
if count(python_path, script_path) == 0
    insert(python_path, int32(0), script_path);
end

% sp_flag

xml_file_path = fullfile(cd,xml_file);

py.PythonVaspRunTest.read_write(pyargs('xml_file',xml_file_path))

% if ~sp_flag
%     py.PythonVaspRunTest.read_write()
% else
%     py.PythonVaspRunTest.read_write_sp(pyargs('xml_file',xml_file))
% end

end