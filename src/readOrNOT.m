function READ_FLAG = readOrNOT(xml_file, mat_file)

% mat_file='all_data.mat';

mat_fullpath = fullfile(cd, mat_file);
xml_fullpath = fullfile(cd, xml_file);
[~,name,ext]=fileparts(mat_file);

if (exist(mat_fullpath,'file')==2)
    
    listing = dir(mat_fullpath);
    matTime = listing.datenum;
    
    listing = dir(xml_fullpath);
    dataTime = listing.datenum;    
    
    if dataTime > matTime
        fprintf(' Newer %s%s file found.\n', name, ext);
        READ_FLAG= true ;
    else
        fprintf(' File %s%s is current.\n', name, ext);
        READ_FLAG= false;
    end
else
    fprintf(' File %s%s does not exist.\n', name, ext);
    READ_FLAG= true;
    
end

end