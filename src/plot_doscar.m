function plot_doscar(all_data_mat_file, dos_prop, fig_prop)

all_data_mat_path = fullfile(cd,all_data_mat_file);
load(all_data_mat_path);

energy_no = length(dos_energy);
ions_no = size(pdos_data,1);
orbital = cellstr(orbital_list);
spin_no=size(pdos_data,2);
fform=sprintf('-d%s', fig_prop.format);
fdpi=sprintf('-r%d', fig_prop.dpi);

for ii=1:length(mat.mat_no)
    ATOM_GROUP(ii)=mat.mat_no(ii);
    ATOM_TAG{ii}=mat.mat_sym(ii,:);
end

colors               = {
                        [ 0.16,     0.44,    1.00 ],...
                        [ 0.93,     0.00,    0.00 ],...
                        [ 0.00,     0.57,    0.00 ],...
                        [ 0.17,     0.17,    0.17 ],...
                        [ 0.44,     0.00,    0.99 ],...
                        [ 1.00,     0.50,    0.10 ],...
                        [ 0.75,     0.00,    0.75 ],...
                        [ 0.50,     0.50,    0.50 ],...
                        [ 0.50,     0.57,    0.00 ],...
                        [ 0.00,     0.00,    0.00 ]
                       };

% this should work for both spin and no-spin plarized cases
if spin_no==2
    sp_spin=2;
else
    sp_spin=1;
end

dos_data_file_name='DOS_data.mat';
dos_data_file_path = fullfile(cd,dos_data_file_name);

READ_FLAG = readOrNOT(all_data_mat_file, dos_data_file_name);

if exist(dos_data_file_path,'file')==2
    MATVariables = who('-file', dos_data_file_path);
else
    MATVariables={};
end

% if the all_data.mat file is newer than DOS_data.mat file and
% the DOS_data.mat file does not have data3/data4/data4 variable,
% then sum them.
% Else, load from DOS_data.mat file.
if READ_FLAG && ~ismember('data6',MATVariables)
    [dos_energy, dos_total, data3, data4, data5, data6] = ...
        doscar_sum(all_data_mat_file, dos_data_file_name);
else
    fprintf(' Reading from DOS data file (%s).\n\n', dos_data_file_name)
    load(dos_data_file_path);
end


switch dos_prop.dos_flag
    
    case 'total'
        
        fig=figure('Units','inches','position',[1 1 2.5 4]);
%         fig=figure;
        get_fig_obj(fig, fig_prop, 'dos');
        axis([0 dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
        
        if sp_spin==1
            plot(dos_total, dos_energy, 'color', colors{1}, 'linewidth', 2);
        elseif sp_spin==2
            plot(dos_total(:,1), dos_energy, 'color', colors{1}, 'linewidth', 2);
            plot(dos_total(:,2), dos_energy, 'color', colors{2}, 'linewidth', 2);
            legend({'Spin up','Spin down'}, 'Location','best');
        end
        
        print('total_dos',fform, fdpi,'-painters');
        logstr=sprintf('Total DOS is plotted in %s format and %d DPI.',...
            fig_prop.format, fig_prop.dpi);
        WriteLog(logstr); 
        
    case 'sp-total'
        
        fig=figure('Units','normalized','position',[.2 .2 .15 .35]);
%         fig=figure;
        get_fig_obj(fig, fig_prop, 'dos');
        axis([0 dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
        
        if sp_spin==1
            error('Only for spin polarized calculation.')
        elseif sp_spin==2
            dos_sp_total = dos_total(:,1) + dos_total(:,2);
            plot(dos_sp_total, dos_energy, 'color', colors{1}, 'linewidth', 2);
%             plot(dos_total(:,2), dos_energy, 'color', colors{2}, 'linewidth', 2);
%             legend({'Spin up','Spin down'}, 'Location','best');
        end
        
        print('total_dos_sp',fform, fdpi,'-painters');
        logstr=sprintf('Orbital resolved DOS is plotted in %s format and %d DPI.',...
            fig_prop.format, fig_prop.dpi);
        WriteLog(logstr);
        
    case 'orbital'
       
        if size(pdos_data,2)==1 || size(pdos_data,2)==4
            
        fig=figure('Units','inches','position',[1 1 2.5 4]);
%         fig=figure;
        get_fig_obj(fig, fig_prop, 'dos');
        axis([0 dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
        
        for ii=1:3
            plot(data3(1, :,ii), dos_energy, 'color',colors{ii}, 'linewidth', 2);        
        end        
        legend({'s','p', 'd'}, 'Location','best');
        
        else
            
        for spin=1:sp_spin
            
        if spin==1
            fig=figure('Units','inches','position',[1 1 4 4]);
%             subplot(1,2,spin)
            get_fig_obj(gca, fig_prop, 'dos-origin');
            axis([-dos_prop.dos_max dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
%             title('Spin up');
            ax = gca;
            ax.YAxisLocation = 'origin';  % setting y axis location to origin
%         else
%             subplot(1,2,spin)
%             get_fig_obj(gca, fig_prop, 'dos');
%             axis([0 dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
%             title('Spin down');
        end    
        
        for ii=1:3
            if spin == 1
                plot(data3(spin,:,ii), dos_energy, 'color',colors{ii}, 'linewidth', 2);     
            else
                plot(-data3(spin,:,ii), dos_energy, 'color',colors{ii}, 'linewidth', 2); 
            end
        end        
        legend({'s','p', 'd'}, 'Location','best');
           
        end
        
        end
        
        print('orbital_resolved_dos',fform, fdpi,'-painters');
        logstr=sprintf('Orbital resolved DOS is plotted in %s format and %d DPI.',...
            fig_prop.format, fig_prop.dpi);
        WriteLog(logstr);
        
        
    case 'sp-orbital'
       
        if size(pdos_data,2)==1
            error('Only for spin polarized calculation.')
        else
            
        fig=figure('Units','inches','position',[1 1 2.5 4]);
%         fig=figure;
        get_fig_obj(fig, fig_prop, 'dos');
        axis([0 dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
        
        for ii=1:3
            plot(data3(1, :,ii)+ data3(1, :,ii), dos_energy, 'color',colors{ii}, 'linewidth', 2);        
        end        
        legend({'s','p', 'd'}, 'Location','best');
        
        print('orbital_resolved_dos_sp',fform, fdpi,'-painters');
        logstr=sprintf('Orbital resolved DOS is plotted in %s format and %d DPI.',...
            fig_prop.format, fig_prop.dpi);
        WriteLog(logstr);
        end        
        
    case 'ionic'
       
        if size(pdos_data,2)==1 || size(pdos_data,2)==4
            
        fig=figure('Units','inches','position',[1 1 2.5 4]);
%         fig=figure;
        get_fig_obj(fig, fig_prop, 'dos');
        axis([0 dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
        
        for inm=1:length(ATOM_TAG)
           plot(data5(1,:,inm),dos_energy, 'color',colors{inm}, 'linewidth', 2);
        end        
        
%         legend({'s','p', 'd'}, 'Location','best');
        
        else
            
        for spin=1:sp_spin
            
        if spin==1
            fig=figure('Units','inches','position',[1 1 4. 4]);
%             subplot(1,2,spin)
            get_fig_obj(gca, fig_prop, 'dos-origin');
            axis([-dos_prop.dos_max dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
            ax = gca;
            ax.YAxisLocation = 'origin';  % setting y axis location to origin
%             title('Spin up');
%         else
%             subplot(1,2,spin)
%             get_fig_obj(gca, fig_prop, 'dos');
%             axis([0 dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
%             title('Spin down');
        end   
        
        for inm=1:length(ATOM_TAG)
            if spin == 1
                plot(data5(spin,:,inm),dos_energy, 'color',colors{inm}, 'linewidth', 2);
            else
                plot(-data5(spin,:,inm),dos_energy, 'color',colors{inm}, 'linewidth', 2);
            end
        end        
        
                
        end
              
        end  
        
        legend(ATOM_TAG, 'Location','best');
        
%         grid on;
        
        print('atom_resolved_dos',fform, fdpi,'-painters');
        logstr=sprintf('Atom resolved DOS is plotted in %s format and %d DPI.',...
            fig_prop.format, fig_prop.dpi);
        WriteLog(logstr);  
        
    case 'sp-ionic'
       
        if size(pdos_data,2)==1
            error('Only for spin polarized calculation.')
        else
            
        fig=figure('Units','inches','position',[1 1 2.5 4]);
%         fig=figure;
        get_fig_obj(fig, fig_prop, 'dos');
        axis([0 dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
        
        for inm=1:length(ATOM_TAG)
           plot(data5(1,:,inm)+data5(2,:,inm),dos_energy, 'color',colors{inm}, 'linewidth', 2);
        end        
        
        
        legend(ATOM_TAG, 'Location','best');
        
        print('atom_resolved_dos_sp',fform, fdpi,'-painters');
        logstr=sprintf('Atom resolved DOS is plotted in %s format and %d DPI.',...
            fig_prop.format, fig_prop.dpi);
        WriteLog(logstr);  
        
        end
        
    case 'd-contrib'
        
        orbital_length = length(orbital);
        
        % optimization
        spd_flag=zeros(1, orbital_length);
        for io=1:orbital_length
            if (strncmpi(char(orbital{io}), 'd',1))
                spd_flag(io)=1;
            end
        end
        
        if size(pdos_data,2)==1 || size(pdos_data,2)==4
            
        fig=figure('Units','inches','position',[1 1 2.5 4]);
%         fig=figure;
        get_fig_obj(fig, fig_prop, 'dos');
        axis([0 dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
        
        ilgnd=1;
        for inm=1:orbital_length
            if spd_flag(inm)==1
                plot(data6(1,:,inm),dos_energy, 'color',colors{inm}, 'linewidth', 2);
                legend_str{ilgnd}=orbital{inm};
                ilgnd = ilgnd+1;
            end
        end      
        
        plot(data3(1,:,3),dos_energy, 'color',colors{ilgnd}, 'linewidth', 2);
        legend_str{ilgnd}='Total d';        
        
%         legend({'s','p', 'd'}, 'Location','best');
        
        else
            
        for spin=1:sp_spin
            
        if spin==1
%             figure('Units','normalized','position',[.1 .1 .35 .35])
            fig=figure('Units','inches','position',[1 1 5.5 4]);
            fig1=subplot(1,2,spin);
            get_fig_obj(gca, fig_prop, 'dos');
            axis([0 dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
            title('Spin up');
        else
            fig2=subplot(1,2,spin);
            get_fig_obj(gca, fig_prop, 'dos');
            axis([0 dos_prop.dos_max dos_prop.Emin dos_prop.Emax]);
            linkaxes([fig1,fig2],'xy');
            title('Spin down');
        end   
        
        ilgnd=1;
        for inm=1:orbital_length
            if spd_flag(inm)==1
                plot(data6(spin,:,inm),dos_energy, 'color',colors{ilgnd}, 'linewidth', 2);
                legend_str{ilgnd}=orbital{inm};
                ilgnd = ilgnd+1;
            end
        end    
        
        plot(data3(spin,:,3),dos_energy, 'color',colors{ilgnd}, 'linewidth', 2);
        legend_str{ilgnd}='Total d';
        
                
        end
              
        end  
        
        legend(legend_str, 'Location','best');
        
        print('dos_d_contribution',fform, fdpi,'-painters');
        logstr=sprintf('Atom resolved DOS is plotted in %s format and %d DPI.',...
            fig_prop.format, fig_prop.dpi);
        WriteLog(logstr);          
        
end

end