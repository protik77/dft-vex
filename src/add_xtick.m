function add_xtick(HSPdata, k_path, this_prop, fig_prop)

colors               = {
                        [ 0.16,     0.44,    1.00 ],...
                        [ 0.93,     0.00,    0.00 ],...
                        [ 0.00,     0.57,    0.00 ],...
                        [ 0.17,     0.17,    0.17 ],...
                        [ 0.44,     0.00,    0.99 ],...
                        [ 1.00,     0.50,    0.10 ],...
                        [ 0.75,     0.00,    0.75 ],...
                        [ 0.50,     0.50,    0.50 ],...
                        [ 0.50,     0.57,    0.00 ],...
                        [ 0.00,     0.00,    0.00 ]
                       };

[mversion, ~] = get_version;
symlen= length(HSPdata);
legend_x = zeros(1,symlen);

for i=1:symlen
    if(i~=1 && i~=symlen) 
        plot([k_path(HSPdata{i}{1}) k_path(HSPdata{i}{1})], ...
            [this_prop.Emin this_prop.Emax],'--','color', colors{4},...
            'linewidth', fig_prop.Linewidth);
    end
    legend_x(i) = k_path(HSPdata{i}{1});
    legend_str{i} = HSPdata{i}{2};
    plot([k_path(1) k_path(end)], [0 0], 'k--', 'linewidth', fig_prop.Linewidth);
end

[legend_x,xind] = sort(legend_x);
legend_str=legend_str(xind);

if mversion >= 9.1 % xticks was introduced in 2016b
    xticks(legend_x)
    xticklabels(legend_str)
elseif mversion < 9.1 && mversion >= 8.4 % from 2014b to 2016b it is XTick
    ax=gca;
    ax.XTick = xticks;
    ax.XTickLabel = xticklabels;
else % for versions before 2014b, one needs to use 'set' to set x axis tick
    warning(' XTick label for this version is not implemented.')
end   

end