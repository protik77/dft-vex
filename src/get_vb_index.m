function vb_index = get_vb_index(eigenvalues_Ef)

bands=size(eigenvalues_Ef,2);

for is=1:size(eigenvalues_Ef,1)
for ib=1:bands
    if max(eigenvalues_Ef(is,ib,:)<0)
        vb_index(is)=ib;
    else
        break;
    end
end
end

end