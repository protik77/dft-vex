function het_vb_cb = get_het_vb_cb_ind(all_data_mat_file, mstar_prop)

    all_data_mat_path = fullfile(cd, all_data_mat_file);

    load(all_data_mat_path);
    
    eigenvalue = eigenvalue-Ef;

    [kpoints, eigenvalue, ~]=check_if_hse(true, incar_tags,...
        kpoints, kpoints_wt, eigenvalue, procar_data);

    clear kpionts_wt procar_data;
    
    clear kpionts_wt procar_data;

    rec_basis = rec_basis * 2 * pi;

%     spin_no = size(eigenvalue,1);
%     kpoints_rec=kpoints*rec_basis;
    nokpoints = size(kpoints,1);
    bands=size(eigenvalue,2);

    for ii=1:length(mat.mat_no)
        ATOM_GROUP(ii)=mat.mat_no(ii);
        ATOM_TAG{ii}=mat.mat_sym(ii,:);
    end
    
    a=1;
    for ig=1:size(ATOM_GROUP,2)    
        gv=ATOM_GROUP(ig);    
        for i=1:gv        
            ATOM_seq{a}=ATOM_TAG{ig};
            a=a+1;
        end    
    end
    clear a ig gv;

    procar_data_file_name='procar_data.mat';
    procar_data_file_path = fullfile(cd, procar_data_file_name);

    READ_FLAG = readOrNOT(all_data_mat_file, procar_data_file_name);

    % if the all_data.mat file is newer than procar_data.mat file and
    % the procar_data.mat file does not have plane_sum variable,
    % then sum them.
    % Else, load from procar_data.mat file.
    if READ_FLAG
        [~, ~, ion_data, ~, ~] ...
            = procar_sum('all_data.mat', procar_data_file_name);
    else
        fprintf(' Reading from procar data file (%s).\n\n', procar_data_file_name)
        load(procar_data_file_path);
    end
    
    ions_no = size(ion_data,4);

    % define color matrix for ion types
    atom_type=size(ATOM_TAG,2);
    cmatrix=nan*ones(nokpoints, bands); % color matrix for atomic procar        
    for b = 1:bands
        for k = 1:nokpoints                           
            val = max(ion_data(1,k,b,:));
            for i = 1:ions_no % for each ion
                if ion_data(1,k,b,i) >= val

                    for it=1:atom_type
                        if ( strcmp(ATOM_seq{i}, ATOM_TAG{it}))
                            cmatrix(k,b)=it;
                        end
                    end
                   break;
                end
            end
        end
    end
    
    
    % get flags for the desired material
    mat_tag = false(size(ATOM_seq));
    for it=1:atom_type
        for ig=1:size(mstar_prop.mat_sym,2)
            if strcmp(ATOM_seq{it}, mstar_prop.mat_sym{ig})
                mat_tag(ig) = it;
            end
        end
    end
    
    [~, vb_index, ~] = get_bandgap(eigenvalue); % get band index of VB
    
    % now the idea is either the maxima or minima will have major
    % contribution from the semiconducting material
    
    % get vb index    
    flag=false;
    for ivb = vb_index:-1:1
        
        [~, max_ind] = max(squeeze(eigenvalue(1,ivb,:)));
        
        for ig=1:size(mstar_prop.mat_sym,2)
            if cmatrix(max_ind, ivb) == mat_tag(ig)
                het_vb_index = ivb;
%                 het_vbm_index = max_ind;
                flag=true;
                break;
            end
        end
        
        if flag
            break;
        end        
    end
    
    % get cb index    
    flag=false;
    for icb = vb_index+1:bands
        
        [~, min_ind] = min(squeeze(eigenvalue(1,icb,:)));
        
        for ig=1:size(mstar_prop.mat_sym,2)
            if cmatrix(min_ind, icb) == mat_tag(ig)
                het_cb_index = icb;
%                 het_cbm_index = min_ind;
                flag=true;
                break;
            end
        end
        
        if flag
            break;
        end        
    end
    
    het_vb_cb = [het_vb_index het_cb_index];

end