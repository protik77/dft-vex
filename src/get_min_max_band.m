function [min_band_ind, max_band_ind] = get_min_max_band(eigenvalues, bs_prop)

% find bands that are in between Emin to Emax.
max_band_ind =[];

for spin = 1:size(eigenvalues,1)
    FLAG_ONCE= false;
    for band = 1:size(eigenvalues,2)
        if( max(eigenvalues(spin, band, :)) >= bs_prop.Emin && ~FLAG_ONCE)
            min_band_ind(spin) = band;
            FLAG_ONCE=true;
        end

        if FLAG_ONCE && ( min(eigenvalues(spin, band, :)) <= bs_prop.Emax) % plot only required ban
            max_band_ind(spin) = band;
        end
    end
end


min_band_ind = min(min_band_ind);

if isempty(max_band_ind)
    max_band_ind = min_band_ind +1;
else
    max_band_ind = max(max_band_ind);
end

end