function READ_FLAG = readOrNOT(force_read, xml_file)

mat_file='all_data.mat';

mat_fullpath = fullfile(cd, mat_file);
xml_fullpath = fullfile(cd, xml_file);

if force_read
    READ_FLAG= true ;
    
elseif (exist(mat_fullpath,'file')==2)
    
    listing = dir(mat_fullpath);
    matTime = listing.datenum;
    
    listing = dir(xml_fullpath);
    dataTime = listing.datenum;    
    
    if dataTime > matTime
        READ_FLAG= true ;
    else
        READ_FLAG= false;
    end
else
    READ_FLAG= true;
    
end

end