function [kpoints, eigenvalues, procar_data] = check_if_hse(print_info, incar_tags, kpoints, kpoints_wt, eigenvalues, procar_data)

nokpoints = size(kpoints,1);

if isfield(incar_tags,'LHFCALC')
    if incar_tags.LHFCALC == 'T'
        
        if print_info
            fprintf(' HSE calculation. Removing IBZ data.\n')
        end
        
        for ik=1:nokpoints
            if(kpoints_wt(ik)==0)
                hse_repeat_ind=ik;
                break;
            end
        end
        
        kpoints=kpoints(hse_repeat_ind:end, :);
        eigenvalues=eigenvalues(:, :, hse_repeat_ind:end);
%         occupation=occupation(:, hse_repeat_ind:end,:);
        procar_data=procar_data(hse_repeat_ind:end, :, :, :, :);
    end
else
    error('LHFCALC tag does not exist in incar_tags variable.')
end


end