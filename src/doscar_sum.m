function [dos_energy, dos_total, data3, data4, data5, data6] = doscar_sum(all_data_mat_file, dos_data_file_name)

all_data_mat_path = fullfile(cd,all_data_mat_file);
load(all_data_mat_path);

energy_no = length(dos_energy);
spin_no=size(pdos_data,2);
ions_no = size(pdos_data,1);

% this should work for both spin and no-spin plarized cases
if spin_no==2
    sp_spin=2;
else
    sp_spin=1;
end

for ii=1:length(mat.mat_no)
    ATOM_GROUP(ii)=mat.mat_no(ii);
    ATOM_TAG{ii}=mat.mat_sym(ii,:);
end

orbital = cellstr(orbital_list);
orbital_length = length(orbital);

% optimization
spd_flag=zeros(1, orbital_length);
for io=1:orbital_length
    if (strncmpi(char(orbital{io}), 's',1))
        spd_flag(io)=1;
    elseif (strncmpi(char(orbital{io}), 'p',1))
        spd_flag(io)=2;
    elseif (strncmpi(char(orbital{io}), 'd',1)) || (strncmpi(char(orbital{io}), 'x',1))
        spd_flag(io)=3;
    else
        error('Error is orbital idenfication part.')
    end
end

data3=zeros(sp_spin,energy_no,3);
data4=zeros(sp_spin,ions_no,energy_no,3);
data6=zeros(sp_spin,energy_no,orbital_length);

for is=1:sp_spin
for ii=1:ions_no
for ie=1:energy_no
    
    data6(is,ie,:)= squeeze(data6(is,ie,:))+squeeze(pdos_data(ii,is,ie,:));
    
for io=1:orbital_length
    if spd_flag(io)==1
        data3(is,ie, 1)= data3(is,ie, 1)+pdos_data(ii,is,ie, io);
        data4(is,ii,ie,1)=data4(is,ii,ie,1)+pdos_data(ii,is,ie,io);
    elseif spd_flag(io)==2
        data3(is,ie, 2)= data3(is,ie, 2)+pdos_data(ii,is,ie, io);
        data4(is,ii,ie,2)=data4(is,ii,ie,2)+pdos_data(ii,is,ie,io);
    elseif spd_flag(io)==3
        data3(is,ie, 3)= data3(is,ie, 3)+pdos_data(ii,is,ie, io);
        data4(is,ii,ie,3)=data4(is,ii,ie,3)+pdos_data(ii,is,ie,io);
    else
        error('Something is wrong in pDOS summation');
    end
end
end
end
end

data5=zeros(sp_spin,energy_no, length(ATOM_TAG));

for is=1:sp_spin
sum_ion=0;
for inm=1:length(ATOM_TAG)

    sum_earl=sum_ion;
    sum_ion=sum_ion+ATOM_GROUP(inm);

    if (inm==1)
       sl=1;
       el=ATOM_GROUP(inm);
    else
        sl=sum_earl+1;
        el=sum_ion;
    end

    for ino=sl:el
    for ie=1:energy_no
        data5(is,ie, inm)= data5(is,ie, inm)+sum(data4(is,ino,ie, :));

    end
    end
end
end

% truncate first value
data3=data3(:,2:end,:);
data4=data4(:,:,2:end,:);
data6=data6(:,2:end,:);
% energy_no=energy_no-1;
dos_energy=dos_energy(2:end)-Ef;
dos_total=dos_total(2:end,:);
data5=data5(:,2:end,:);

save(dos_data_file_name, 'dos_total', 'dos_energy','data3','data4','data5','data6');

end