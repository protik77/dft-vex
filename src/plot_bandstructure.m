function plot_bandstructure(all_data_mat_file, fig_prop, bs_prop)

colors               = {
                        [ 0.16,     0.44,    1.00 ],...
                        [ 0.93,     0.00,    0.00 ],...
                        [ 0.00,     0.57,    0.00 ],...
                        [ 0.17,     0.17,    0.17 ],...
                        [ 0.44,     0.00,    0.99 ],...
                        [ 1.00,     0.50,    0.10 ],...
                        [ 0.75,     0.00,    0.75 ],...
                        [ 0.50,     0.50,    0.50 ],...
                        [ 0.50,     0.57,    0.00 ],...
                        [ 0.00,     0.00,    0.00 ]
                       };
                   
all_data_mat_path = fullfile(cd,all_data_mat_file);
load(all_data_mat_path, 'kpoints', 'eigenvalue', 'Ef', 'kpoints_wt',...
    'procar_data', 'incar_tags');

eigenvalues = eigenvalue-Ef;

[kpoints, eigenvalues, ~]=check_if_hse(true, incar_tags, ...
    kpoints, kpoints_wt, eigenvalues, procar_data);

[k_path, HSPdata] = get_Kpath(kpoints, true);
[from_band, to_band]=get_min_max_band(eigenvalues, bs_prop);

clear eigenvalue;

if size(eigenvalues,1) == 1
    
    fig=figure('Units','inches','position',[1 1 6 4]);
    get_fig_obj(fig, fig_prop, 'bs');
    axis([k_path(1) k_path(end) bs_prop.Emin bs_prop.Emax]);
    
    for b = from_band:to_band
        plot(k_path, squeeze(eigenvalues(1,b,:)),'color', colors{1},...
            'linewidth', fig_prop.Linewidth);
    end
    
    symlen = length(HSPdata);
    legend_x = zeros(1,symlen);
    for i=1:symlen
        if(i~=1 && i~=symlen) 
            plot([k_path(HSPdata{i}{1}) k_path(HSPdata{i}{1})], ...
                [bs_prop.Emin bs_prop.Emax],'--','color', colors{4},...
                'linewidth', fig_prop.Linewidth);
        end
    end
    
    add_xtick(HSPdata, k_path, bs_prop, fig_prop);  
    
elseif size(eigenvalues,1) == 2

    if isfield(bs_prop, 'bs_flag') && strcmp(bs_prop.bs_flag, 'combined-sp')
        
    fig=figure('Units','inches','position',[1 1 6 4]);
    get_fig_obj(fig, fig_prop, 'bs');
    axis([k_path(1) k_path(end) bs_prop.Emin bs_prop.Emax]);
        
    for spin=1:size(eigenvalues,1)

        if spin==1
            for b = from_band:to_band
                plot(k_path, squeeze(eigenvalues(spin,b,:)),'color', colors{1},...
                    'linewidth', fig_prop.Linewidth);
            end
        elseif spin==2
            for b = from_band:to_band
                plot(k_path, squeeze(eigenvalues(spin,b,:)),'color', colors{2},...
                    'linewidth', fig_prop.Linewidth);
            end
            
            add_xtick(HSPdata, k_path, bs_prop, fig_prop);
        end

    end

    else
        
    for spin=1:size(eigenvalues,1)
        if spin==1
            figure('Units','inches','position',[1 1 12 8]);
            fig1=subplot(1,2,spin);
            get_fig_obj(gca, fig_prop, 'bs');
        else
            fig2=subplot(1,2,spin);
            linkaxes([fig1,fig2],'xy');
            get_fig_obj(gca, fig_prop, 'bs');
        end

        if spin==1
            title('Spin up');
        else
            title('Spin down');
        end  
        axis([k_path(1) k_path(end) bs_prop.Emin bs_prop.Emax]);
        for b = from_band:to_band
            plot(k_path, squeeze(eigenvalues(spin,b,:)),'color', colors{1},...
                'linewidth', fig_prop.Linewidth);
        end
        
%         symlen = length(HSPdata);
%         legend_x = zeros(1,symlen);        
%         for i=1:symlen
%             if(i~=1 && i~=symlen) 
%                 plot([k_path(HSPdata{i}{1}) k_path(HSPdata{i}{1})], ...
%                     [bs_prop.Emin bs_prop.Emax],'--','color', colors{4},...
%                     'linewidth', fig_prop.Linewidth);
%             end
%             legend_x(i) = k_path(HSPdata{i}{1});
%             legend_str{i} = HSPdata{i}{2};
%             if isfield(bs_prop,'partial') && bs_prop.partial
%                 if i==1
%                     legend_str{i} = sprintf('(%0.4f, %0.4f)', kpoints(1,1),kpoints(1,2));
%                 end
%                 
%                 if i==symlen
%                     legend_str{i} = sprintf('(%0.4f, %0.4f)', kpoints(end,1),kpoints(end,2));
%                 end
%             end                 
%             plot([k_path(1) k_path(end)], [0 0], 'k--', 'linewidth', fig_prop.Linewidth);
%         end
        
        add_xtick(HSPdata, k_path, bs_prop, fig_prop);  
    end
    end
    
end

fform=sprintf('-d%s', fig_prop.format);
fdpi=sprintf('-r%d', fig_prop.dpi);
print('bandstructure',fform, fdpi);
logstr=sprintf('Bandstructure saved in %s with %d DPI. EMAX=%2.2f EMIN=%2.2f.', ...
    fig_prop.format, fig_prop.dpi,bs_prop.Emax, bs_prop.Emin);
WriteLog(logstr); 


end