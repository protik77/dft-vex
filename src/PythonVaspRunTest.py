import xml.etree.ElementTree as et
import numpy as np
import scipy.io as sio

def read_incar_tags(root):

    default_incar_tags = {'LORBIT': 0,
                         'LSORBIT': 'F',
                          'LHFCALC':'F',
                         'ICHARG': '11',
                         'ISPIN': '1',
                         }

    for child in root:

        if child.tag == 'incar':

            incar_tags = child.findall('i')

            for tags in incar_tags:

                tag = tags.get('name')

                tag_val = tags.text

                if tag in default_incar_tags:
                    default_incar_tags[tag] = tag_val.strip()
                    # print(tag, needed_incar_tags[tag])
            break

    return default_incar_tags

def read_structure_info(root):
    mat_no = []
    mat_sym = []

    for child in root:

        if child.tag == 'atominfo':

            for c in child:

                if c.tag == 'atoms':
                    total_atoms = int(c.text)

                if c.tag == 'array' and c.get('name') == 'atomtypes':

                    for c2 in c:

                        if c2.tag == 'set':

                            for c3 in c2:

                                if c3.tag == 'rc':

                                    for idx, c4 in enumerate(c3):

                                        if idx == 0:
                                            mat_no.append(int(c4.text))
                                        if idx == 1:
                                            mat_sym.append(c4.text)

        if child.tag == 'structure' and child.get('name')=='initialpos':

            basis = []
            rec_basis = []

            for c1 in child:

                if c1.tag == 'crystal':

                    for c2 in c1:

                        if c2.tag == 'varray' and c2.get('name') == 'basis':

                            for c3 in c2:

                                if c3.tag == 'v' :

                                    basis_this = c3.text

                                    basis_temp = basis_this.split()

                                    kp_temp = [float(i) for i in basis_temp]

                                    basis.append(kp_temp)

                        if c2.tag == 'i' and c2.get('name') == 'volume':

                            volume = float(c2.text.split()[0])

                        if c2.tag == 'varray' and c2.get('name') == 'rec_basis':

                            for c3 in c2:

                                if c3.tag == 'v' :

                                    basis_this = c3.text

                                    basis_temp = basis_this.split()

                                    kp_temp = [float(i) for i in basis_temp]

                                    rec_basis.append(kp_temp)

            basis = np.array(basis)
            rec_basis = np.array(rec_basis)
            # break

    return mat_no, mat_sym, total_atoms, volume, basis, rec_basis

def read_kpoints(root):
    kpoints = []
    kpoints_wt = []

    for child in root:

        if child.tag == 'kpoints':

            for c in child:

                if c.get('name') == 'kpointlist':

                    for kp in c:
                        kp_temp = []

                        kp_this = kp.text

                        kp_temp = kp_this.split()

                        kp_temp = [float(i) for i in kp_temp]

                        kpoints.append(kp_temp)


                elif c.get('name') == 'weights':

                    for kp in c:
                        kp_temp = []

                        kp_this = kp.text

                        kp_temp = kp_this.split()

                        kp_temp = [float(i) for i in kp_temp]

                        kpoints_wt.append(kp_temp)

            kpoints_no = len(kpoints)
            kpoints = np.array(kpoints)
            kpoints_wt = np.array(kpoints_wt)
            # break

    return kpoints_no, kpoints, kpoints_wt

def read_eigenvalues(root, kpoints_no, spin_polarized=False):
    initialize_eiganval = True
    num_bands = 0

    for child in root:

        if child.tag == 'calculation':

            for c in child:

                if c.tag == 'eigenvalues':

                    for c1 in c:

                        if c1.tag == 'array':

                            for c2 in c1:

                                if c2.tag == 'set':

                                    for idx1, spin in enumerate(c2):

                                        if spin.tag == 'set':

                                            if spin.attrib['comment'] == 'spin 1':
                                                spin_ind = 1
                                            elif spin.attrib['comment'] == 'spin 2':
                                                spin_ind = 2

                                            for idx2, kp in enumerate(spin):

                                                eig_col = []

                                                for band in kp:

                                                    # initialize eigenval array
                                                    if idx1 == 0 and idx2 == 0:

                                                        if band.tag == 'r':
                                                            num_bands = num_bands + 1

                                                    eig_this = band.text

                                                    eig_temp = eig_this.split()

                                                    kp_temp = [float(i) for i in eig_temp]

                                                    eig_col.append(kp_temp)

                                                if initialize_eiganval:

                                                    if spin_polarized:
                                                        eigenval = np.zeros((2, num_bands, kpoints_no))
                                                        occ = np.zeros((2, num_bands, kpoints_no))
                                                        initialize_eiganval = False
                                                    else:
                                                        eigenval = np.zeros((1, num_bands, kpoints_no))
                                                        occ = np.zeros((1, num_bands, kpoints_no))
                                                        initialize_eiganval = False

                                                eig_col = np.array(eig_col)

                                                eigenval[spin_ind-1, :, idx2] = eig_col[:, 0]
                                                occ[spin_ind-1, :, idx2] = eig_col[:, 1]
            # break
        # else:
        #     child.clear()
    return num_bands, eigenval, occ

# @timefunc
def read_dos(root, total_atoms):
    dos_list = []
    orbital_list = []
    initialize_dos = True
    initialize_pdos = True

    for child in root:
        if child.tag == 'calculation':

            for c in child:

                if c.tag == 'dos':

                    # capture fermi level
                    for c1 in c:

                        if c1.tag == 'i' and c1.get('name') == 'efermi':

                            Ef = float(c1.text.split()[0])

                        if c1.tag == 'total':

                            for c2 in c1:

                                if c2.tag == 'array':

                                    for c3 in c2:

                                        if c3.tag == 'set':

                                            for idx1, spins in enumerate(c3):

                                                if initialize_dos:
                                                    no_spins_dos = len(c3)
                                                    no_dos_energies = len(spins)

                                                dos_list = []

                                                for idx2, spin in enumerate(spins):

                                                    if spin.tag == 'r':
                                                        dos_this = spin.text

                                                        dos_temp = dos_this.split()

                                                        kp_temp = [float(i) for i in dos_temp]

                                                        dos_list.append(kp_temp)

                                                dos_list = np.array(dos_list)

                                                if idx1 == 0:
                                                    dos_energy = dos_list[:, 0]

                                                if initialize_dos:
                                                    dos_total = np.zeros((no_dos_energies, no_spins_dos))
                                                    dos_integrated = np.zeros((no_dos_energies, no_spins_dos))
                                                    initialize_dos = False

                                                dos_total[:, idx1] = dos_list[:, 1]
                                                dos_integrated[:, idx1] = dos_list[:, 2]

                        if c1.tag == 'partial':

                            for c2 in c1:

                                if c2.tag == 'array':

                                    for c3 in c2:
                                        if c3.tag == 'field':
                                            if c3.text.split()[0] != 'energy':
                                                orbital_list.append(c3.text.split()[0])

                                        if c3.tag == 'set':

                                            for idx1, atom in enumerate(c3):  # in atoms

                                                for idx2, spin in enumerate(atom):

                                                    pdos_list = []

                                                    for energy in spin:

                                                        if energy.tag == 'r':
                                                            pdos_this = energy.text

                                                            pdos_temp = pdos_this.split()

                                                            kp_temp = [float(i) for i in pdos_temp]

                                                            pdos_list.append(kp_temp)

                                                    pdos_list = np.array(pdos_list)

                                                    pdos_list = pdos_list[:, 1:]

                                                    # print(pdos_list.shape)

                                                    if initialize_pdos:
                                                        pdos_data = np.zeros((
                                                                             total_atoms, no_spins_dos, no_dos_energies,
                                                                             len(orbital_list)))
                                                        initialize_pdos = False
                                                        # print(pdos_data.shape)

                                                    pdos_data[idx1, idx2, :, :] = pdos_list
                    # break

    return Ef, dos_energy, dos_total, dos_integrated, pdos_data, orbital_list

def read_procar(root, kpoints_no, num_bands, total_atoms, orbital_no):
    initialize_procar_data = True

    for child in root:

        if child.tag == 'calculation':

            for c in child:

                if c.tag == 'projected':

                    # read PROCAR data

                    for array in c:

                        if array.tag == 'array':

                            for set in array:

                                if set.tag == 'set':

                                    # if initialize_procar_data:
                                    #     procar_data = np.zeros(
                                    #         (kpoints_no, num_bands, 4, total_atoms, orbital_no))
                                    #     initialize_procar_data = False

                                    for idx1, spin in enumerate(set):

                                        if initialize_procar_data:
                                            no_spins_procar = len(set)
                                            # print(no_spins_procar)
                                            procar_data = np.zeros(
                                                (kpoints_no, num_bands, no_spins_procar, total_atoms, orbital_no))
                                            initialize_procar_data = False

                                        for idx2, kp in enumerate(spin):

                                            for idx3, band in enumerate(kp):

                                                for idx4, atom in enumerate(band):

                                                    pro_this = atom.text
                                                    pro_temp = pro_this.split()
                                                    kp_temp=[]
                                                    for i in pro_temp:
                                                        try:
                                                            kp_temp.append(float(i))
                                                        except ValueError:
                                                            kp_temp.append(0.0)
                                                        # if i == '*******':
                                                        #     kp_temp.append(0.0)
                                                        # else:
                                                        #     kp_temp.append(float(i))

                                                    # kp_temp = [float(i) for i in pro_temp]
                                                    kp_temp = np.array(kp_temp)

                                                    procar_data[idx2, idx3, idx1, idx4, :] = kp_temp
    return procar_data


def save_data(root, filename='all_data.mat'):

    incar_tags = read_incar_tags(root)

    mat_no, mat_sym, total_atoms, volume, basis, rec_basis = read_structure_info(root)

    kpoints_no, kpoints, kpoints_wt = read_kpoints(root)

    if incar_tags['ISPIN'] =='2':
        num_bands, eigenval, occ = read_eigenvalues(root, kpoints_no, spin_polarized=True)
    else:
        num_bands, eigenval, occ = read_eigenvalues(root, kpoints_no)

    Ef, dos_energy, dos_total, dos_integrated, pdos_data, orbital_list = read_dos(root, total_atoms)

    procar_data = read_procar(root, kpoints_no, num_bands, total_atoms, len(orbital_list))

    mat = {'mat_sym': mat_sym, 'mat_no': mat_no}

    sio.savemat(filename, {'incar_tags': incar_tags,
                                 'mat': mat,
                                 'kpoints': kpoints,
                                 'kpoints_wt': kpoints_wt,
                                 'kpoints_no': kpoints_no,
                                 'total_atoms': total_atoms,
                                 'dir_basis': basis,
                                 'rec_basis': rec_basis,
                                 'volume': volume,
                                 'eigenvalue': eigenval,
                                 'occupation': occ,
                                 'bands': num_bands,
                                 'Ef': Ef,
                                 'dos_energy': dos_energy,
                                 'dos_total': dos_total,
                                 'dos_integrated': dos_integrated,
                                 'orbital_list': orbital_list,
                                 'pdos_data': pdos_data,
                                 'procar_data': procar_data,
                                 },
                do_compression=True,
                )


def read_write(xml_file='vasprun.xml'):
    tree = et.parse(xml_file)
    root = tree.getroot()
    save_data(root)
