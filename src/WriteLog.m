function WriteLog(Data)

persistent FID

filename='LogFile.txt';

% check if file exists
if exist(filename, 'file') == 2
    FID = fopen(fullfile(filename), 'a');
else    
    FID = fopen(fullfile(filename), 'w');
end

% throw error is file opening have issue
if FID < 0
    error('Cannot open file');
end
  
% write data
fprintf(FID, '%s\t%s\n', datestr(now),Data);

% close file
fclose(FID);
FID = -1;

end

