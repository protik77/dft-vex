function XMLReadMatlab(xml_file)

xDoc = xmlread(xml_file);

rootNode= xDoc.getDocumentElement;
childNodes=rootNode.getChildNodes;
clear xDoc;

% flags
incar_tags.LSORBIT='F';
incar_tags.ISPIN='1';
incar_tags.LHFCALC='F';
incar_tags.ICHARG='11';
incar_tags.LORBIT='0';


% determine atom names & numbers & check if HSE turned on

for ic=0:childNodes.getLength-1
    
    if strcmpi(childNodes.item(ic).getNodeName, 'incar')
        incarChilds=childNodes.item(ic).getChildNodes;
        
        for ii=0:incarChilds.getLength-1
            if strcmpi(incarChilds.item(ii).getNodeName, 'i')
                allAttributes=incarChilds.item(ii).getAttributes;
                lengthh=allAttributes.getLength;
                if(lengthh==2)
                type2={char(allAttributes.item(lengthh-2).getName)};
                    if(strcmpi(type2,'name'))
                        type3={char(allAttributes.item(lengthh-2).getValue)};
                        
                        if(strcmpi(type3,'LSORBIT'))
                            soc_str=strtrim(char(incarChilds.item(ii).getTextContent));
                            if (strcmpi(soc_str, 'T'))
                                incar_tags.LSORBIT='T';
                            end
                        end
                        
                        if(strcmpi(type3,'ISPIN'))
                            soc_str=strtrim(char(incarChilds.item(ii).getTextContent));
                            if (strcmpi(soc_str, '2'))
                                incar_tags.ISPIN='2';
                            end
                        end                        
                        
                        if(strcmpi(type3,'LHFCALC'))
                            hse_str=strtrim(char(incarChilds.item(ii).getTextContent));
                            if (strcmpi(hse_str,'T'))
                                incar_tags.LHFCALC='T';
                            end
                        end
                        
                        if(strcmpi(type3,'LORBIT'))
                            hse_str=strtrim(char(incarChilds.item(ii).getTextContent));
                            incar_tags.LORBIT=hse_str;
                        end                        
                        
%                         if(strcmpi(type3,'LDIPOL'))
%                             efield_str=strtrim(char(incarChilds.item(ii).getTextContent));
%                             if (strcmpi(efield_str,'T'))
%                                 EFIELD_FLAG=1;
%                             end
%                         end
%                         
%                         if(EFIELD_FLAG==1 && strcmpi(type3,'IDIPOL'))
%                             efield_dir=strtrim(char(incarChilds.item(ii).getTextContent));
%                             efield_dir=str2double(efield_dir);
%                         end


                    end
%                 elseif(lengthh==1 && EFIELD_FLAG ==1)
%                     type2={char(allAttributes.item(lengthh-1).getName)};
%                     
%                         if(strcmpi(type2,'name'))
%                             type3={char(allAttributes.item(lengthh-1).getValue)};                    
%                     
%                             if (strcmpi(type3,'DIP%EFIELD'))
%                                 efield_val=strtrim(char(incarChilds.item(ii).getTextContent));
%                                 efield_val=str2double(efield_val);
%                             end     
%                         
%                         end
                end
            end
        end
        
    end
end
    
    clear type2 type3 hse_str soc_str;

% read kpoints
for k=0:childNodes.getLength-1
   if strcmpi(childNodes.item(k).getNodeName, 'kpoints')
       insidechildNodes=childNodes.item(k).getChildNodes;


       for ks=0:insidechildNodes.getLength-1

%             if (strcmp(incar_tags.LHFCALC, 'F'))
%                 if strcmpi(insidechildNodes.item(ks).getNodeName, 'generation')
%                     divNode=insidechildNodes.item(ks).getChildNodes;
% 
%                     for id=0:divNode.getLength-1
%                         if strcmpi(divNode.item(id).getNodeName, 'i')
%                             % divs: # of kpoints along each line
%                             divs=divNode.item(id).getTextContent;
%                             divs=str2num(char(divs));
%                         end
%                     end
%                 end
%             elseif (strcmp(incar_tags.LHFCALC, 'T'))
%                 % if HSE is turned on, set the value manually
%                 % 4 sections means there are 4 lines
%                 % needed for m* calculation
%                 kpoints_sections=4;
%             end

            if strcmpi(insidechildNodes.item(ks).getNodeName, 'varray')

                kAttr=insidechildNodes.item(ks).getAttributes;

                if(strcmp(kAttr.item(0).getValue,'kpointlist'))
                    kdata=insidechildNodes.item(ks).getTextContent;
                    temp_data=str2num(kdata);  
                    kpoints=temp_data;
                    v_ind=k;
                end

                if(strcmp(kAttr.item(0).getValue,'weights'))
                    kdata=insidechildNodes.item(ks).getTextContent;
                    temp_data=str2num(kdata);  
                    kpoints_wt=temp_data;
                    v_ind=k;
                end
            end

       end
   end
end

clear insidechildNodes;

kpoints_no=int32(size(kpoints,1));

% clear divNode;    
for ic=0:childNodes.getLength-1
if strcmpi(childNodes.item(ic).getNodeName, 'atominfo')
    atomChilds=childNodes.item(ic).getChildNodes;

    for ia=0:atomChilds.getLength-1
        if strcmpi(atomChilds.item(ia).getNodeName, 'array')

            childAttributes=atomChilds.item(ia).getAttributes;

            lengthc=childAttributes.getLength;

            ind2=1;

            if(lengthc==1)
                type={char(childAttributes.item(lengthc-1).getValue)};
            else
                fprintf('ERROR: cannot get atom names.');
                return;
            end

            if (strcmpi(type,'atomtypes'))
                reachedNode=atomChilds.item(ia).getChildNodes;

                for is=0:reachedNode.getLength-1
                    if strcmpi(reachedNode.item(is).getNodeName, 'set')
                        dataNode=reachedNode.item(is).getChildNodes;


                        for irc=0:dataNode.getLength-1
                            if strcmpi(dataNode.item(irc).getNodeName, 'rc')

                                rcNode=dataNode.item(irc).getChildNodes;
                                ind=1;
                                for icc=0:rcNode.getLength-1
                                   if strcmpi(rcNode.item(icc).getNodeName, 'c')
                                       data=rcNode.item(icc).getTextContent;                                          

                                       if(ind==1)
                                           mat.mat_no(ind2)=str2double(data);
                                       elseif(ind==2)
                                           mat.mat_sym{ind2}=char(data);
                                           ind2=ind2+1;
                                       end
                                       ind=ind+1;


                                       if (ind==3)
                                           break;
                                       end

                                   end
                                end

                            end
                        end
                    end
                end

                break;                    
            end

        end
    end

end

clear data;
    
if strcmpi(childNodes.item(ic).getNodeName, 'structure')
    crystalNode=childNodes.item(ic).getChildNodes;

    for ia=0:crystalNode.getLength-1
        if strcmpi(crystalNode.item(ia).getNodeName, 'crystal')

            varrayNode=crystalNode.item(ia).getChildNodes;

            for iv=0:varrayNode.getLength-1

 
                if strcmpi(varrayNode.item(iv).getNodeName, 'i')
                    temp_data=varrayNode.item(iv).getTextContent;
                    volume=str2num(temp_data);
                end

                if strcmpi(varrayNode.item(iv).getNodeName, 'varray')
                childAttributes=varrayNode.item(iv).getAttributes;
                lengthc=childAttributes.getLength;

                if(lengthc==1)
                    type={char(childAttributes.item(lengthc-1).getValue)};
                else
                    fprintf('ERROR: cannot get basis.\n');
                    return;
                end

                if (strcmpi(type,'basis'))
                    temp_data=varrayNode.item(iv).getTextContent;

                    temp_data=str2num(temp_data);  
                    dir_basis=temp_data;

                end
                end                 

                if strcmpi(varrayNode.item(iv).getNodeName, 'varray')
                childAttributes=varrayNode.item(iv).getAttributes;
                lengthc=childAttributes.getLength;

                if(lengthc==1)
                    type={char(childAttributes.item(lengthc-1).getValue)};
                else
                    fprintf('ERROR: cannot get reciprocal basis.\n');
                    return;
                end

                if (strcmpi(type,'rec_basis'))
                    temp_data=varrayNode.item(iv).getTextContent;

                    temp_data=str2num(temp_data);  
                    rec_basis=temp_data;

                end
                end
            end
        end
    end
end
    
    
end

clear childNodes atomChilds childAttributes reachedNode rcNode dataNode ...
    crystalNode varrayNode allAttributes type;

% if (EFIELD_FLAG ~= 1)
%     % set some default value if efield is not applied
%     efield_val=0.0;
%     efield_sstr='no';
% end


[calcNode, ~]=findChildNode(rootNode, 'calculation');

[eigNode, ~]=findChildNode(calcNode, 'eigenvalues');

try
    [dosNode, ~]=findChildNode(calcNode, 'dos');
catch
   fprintf('\nDOS is not calculated in this calculation.\n');
end

[arrayNode, ~]=findChildNode(eigNode, 'array');

[setNode, ~]=findChildNode(arrayNode, 'set');

[eigsNode, ~]=findChildNode(setNode, 'set');

clear eigNode arrayNode;

dosNodeChilds=dosNode.getChildNodes;

for ind=0:dosNodeChilds.getLength-1

    if strcmpi(dosNodeChilds.item(ind).getNodeName, 'i')

        point = dosNodeChilds.item(ind).getTextContent;
        eig_v = str2num(point);
        Ef=eig_v;
    end
end

eigenNodes=eigsNode.getChildNodes;


bands=int32((eigenNodes.item(v_ind).getLength-1)/2);

if strcmp(incar_tags.ISPIN,'2')
    spin=2;
else
    spin=1;
end

eigenvalue=zeros(spin,bands,kpoints_no);  
occupation=zeros(spin,bands,kpoints_no);  

ispin=1;
for is=0:setNode.getLength-1
    if strcmpi(setNode.item(is).getNodeName, 'set')
        
        col=1;
        eig2Node=setNode.item(is).getChildNodes;

        for ie=0:eig2Node.getLength-1   

            eNode=eig2Node.item(ie);

            bandsNode=eNode.getChildNodes;   

            if strcmpi(eNode.getNodeName, 'set')  

                temp_data=bandsNode.getTextContent;
                temp_data=str2num(temp_data);  

                eigenvalue(ispin,:,col)=temp_data(:,1);
                occupation(ispin,:,col)=temp_data(:,2);

                col=col+1;

            end
        end
        
        if is == spin+1
            break;
        else
            ispin=ispin+1;
        end
    end

end
clear eNode eigsNode eigenNodes bandsNode eig_v col row ik ind k setNode...
    eig2Node ispin; 

    
% read procar data
[projectedNode, ~]=findChildNode(calcNode, 'projected');

[arrayNode, ~]=findChildNode(projectedNode, 'array');

[setNode1, ~]=findChildNode(arrayNode, 'set');

node=arrayNode.getChildNodes;

ind=1;
for i=0:node.getLength-1
    if strcmpi(node.item(i).getNodeName, 'field')
        orbital_list{ind}= strtrim(char(node.item(i).getTextContent));
        ind=ind+1;
    end
end

orbital_no=ind-1;

clear projectedNode arrayNode;

% initialize data matrix
magnetzn=1;
FLAG_ONCE=0;
for ik=0:setNode1.getLength-1

    if strcmpi(setNode1.item(ik).getNodeName, 'set')

        spinNode=setNode1.item(ik).getChildNodes;

%             for is=0:spinNode.getLength-1
        for is=0:2

            if (strcmpi(spinNode.item(is).getNodeName, 'set') && FLAG_ONCE~=1)

                kpointsNode=spinNode.item(is).getChildNodes;

                band=1;

                for ib=0:2

                    if strcmpi(kpointsNode.item(ib).getNodeName, 'set')
                        bandsNode= kpointsNode.item(ib).getChildNodes;

                        ion=1;                       

                        for ii=0:bandsNode.getLength-1                            

                            if strcmpi(bandsNode.item(ii).getNodeName, 'r')
                                ion=ion+1;
                            end
                        end

                        band=band+1;
                    end
                    FLAG_ONCE=1;
                end
            end

        end

        magnetzn=magnetzn+1;

    end

end

clear band;

ions_no=ion-1;
magnet_no=magnetzn-1;

FLAG=0;
    
procar_data=zeros(kpoints_no, bands, magnet_no, ions_no, orbital_no);


% variables initialization
magnetzn=1;

clear setNode bandsNode;
clear dosNode dosNodeChilds incarChilds kAttr kdata node rootNode;

for ik=0:setNode1.getLength-1

    if strcmpi(setNode1.item(ik).getNodeName, 'set')

        spinNode=setNode1.item(ik).getChildNodes;

        kpoint=1;

        for is=0:spinNode.getLength-1

            if strcmpi(spinNode.item(is).getNodeName, 'set')

                kpointsNode=spinNode.item(is).getChildNodes;

                band=1;

                for ib=0:kpointsNode.getLength-1

                    if strcmpi(kpointsNode.item(ib).getNodeName, 'set')

                        temp_data=kpointsNode.item(ib).getTextContent;

                        temp_data=str2num(temp_data);
                        
                        procar_data(kpoint, band, magnetzn, :, :)=temp_data(:,:);                                                               


                        band=band+1;
                    end

                    if(FLAG==1)
                        break;
                    end
                end

                kpoint=kpoint+1;
            end
            if(FLAG==1)
                break;
            end
        end

        magnetzn=magnetzn+1;

    end

end

[dosNode, ~]=findChildNode(calcNode, 'dos');

clear calcNode kpointsNode;

[totalNode, ~]=findChildNode(dosNode, 'total');

[arrayNode, ~]=findChildNode(totalNode, 'array');

[setNode1, ~]=findChildNode(arrayNode, 'set');

spinNode=setNode1.getChildNodes;

dosNodeChilds=dosNode.getChildNodes;

for ind=0:dosNodeChilds.getLength-1

    if strcmpi(dosNodeChilds.item(ind).getNodeName, 'i')

        point = dosNodeChilds.item(ind).getTextContent;
        eig_v = str2num(point);
        Ef=eig_v;
    end
end
clear eig_v point;
% number of energies for initialization
energy_ind=1;
magnetzn=1;
for is=0:2

    if strcmpi(spinNode.item(is).getNodeName, 'set')

        tdosNode=spinNode.item(is).getChildNodes;

        for id=0:tdosNode.getLength-1

            if strcmpi(tdosNode.item(id).getNodeName, 'r')
                energy_ind=energy_ind+1;
            end

        end

        magnetzn=magnetzn+1;
    end

end

energy_no=energy_ind-1;

dos_energy=zeros(1,energy_no);
dos_total=zeros(energy_no, spin);
dos_integrated=zeros(energy_no, spin);

FLAG=0;
FLAG_ONCE=0;
magnetzn=1;
for is=0:spinNode.getLength-1

    if strcmpi(spinNode.item(is).getNodeName, 'set')

        temp_data=spinNode.item(is).getTextContent;

        % sometimes the first value is with ***** so this            

        temp_data2 = textscan(char(temp_data),'%f%f%f', 'TreatAsEmpty', '**********', 'CollectOutput', 1);

        temp_data3 = temp_data2{1};

        if (isnan(temp_data3(1,2)==1))
            temp_data3=temp_data3(2:end,:);
        end

        if FLAG_ONCE==0
            dos_energy(magnetzn,:)= temp_data3(:,1)';
            FLAG_ONCE = 1;
        end
        dos_total(:, magnetzn)=temp_data3(:,2);
        dos_integrated(:, magnetzn)=temp_data3(:,3);

        magnetzn=magnetzn+1;
    end

    if(FLAG==1)
        break;
    end

end

% E_dos=E_dos(2:end);
% total_dos=total_dos(2:end);   

clear dosNodeChilds temp_data temp_data2 temp_data3 E_dos ...
    energy_ind tdosNode totalNode spinNode;

[partialNode, ~]=findChildNode(dosNode, 'partial');

[arrayNode, ~]=findChildNode(partialNode, 'array');

[setNode1, ~]=findChildNode(arrayNode, 'set');

ionNode=setNode1.getChildNodes;

clear partialNode arrayNode setNode1 dosNode;

pdos_data=zeros(ions_no, magnet_no, energy_no, orbital_no);   

clear energy_no;

FLAG=0;
ion_ind=1;
for ii=0:ionNode.getLength-1

    if strcmpi(ionNode.item(ii).getNodeName, 'set')

        spinNode=ionNode.item(ii).getChildNodes;

        spin_ind=1;

        for is=0:spinNode.getLength-1

            if strcmpi(spinNode.item(is).getNodeName, 'set')

                temp_data=spinNode.item(is).getTextContent;

                temp_data=str2num(temp_data);

                pdos_data(ion_ind, spin_ind,:,:)=temp_data(:,2:end); % from 2 to truncate energy

                spin_ind=spin_ind+1;

            end

        end

        ion_ind=ion_ind+1;
    end

end

clear ionNode spinNode

clear FLAG FLAG_ONCE i ia ib ic icc id ie ii ik ind ind2 ion ion_ind irc...
    is iv kpoint ks lengthc lengthh magnet_no magnetzn orbital_no spin ...
    spin_count xml_file band spin_ind temp_data v_ind ions_no;

mat.mat_sym = char(mat.mat_sym);
orbital_list = char(orbital_list);
total_atoms = int32(sum(mat.mat_no));

save('all_data.mat');
    

end