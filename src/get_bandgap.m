function [bg, vb_index, vbm_cbm_index] = get_bandgap(eigenvalues)

bands=size(eigenvalues,2);

for is=1:size(eigenvalues,1)
for ib=1:bands
    if max(eigenvalues(is,ib,:)<0)
        ivb=ib;
    else
        break;
    end
bg(is) = min(eigenvalues(is, ivb+1,:)) - max(eigenvalues(is, ivb,:));
vb_index(is) = ivb;

end
end

for is=1:size(eigenvalues,1)
    [~, ind] = max(eigenvalues(is,vb_index(is),:));
    vbm_cbm_index(is,1) = ind;


    [~, ind] = min(eigenvalues(is,vb_index(is)+1,:));
    vbm_cbm_index(is,2) = ind;

end

% bg

end