clearvars

xml_file='vasprun.xml';
FORCE_READ=true;
FORCE_READ=false;
mat_file='all_data.mat';

% use matlab or python for xml?
python_or_matlab(xml_file, mat_file, FORCE_READ);

% Ef=4.79;    % pbe 1t tase2 ef
% Ef=4.7646;  % hse 1t tase2 ef
% Ef=8.6793;      % PBE FCC-Au ef
% Ef=3.2285;  % pbe 2t tase2 ef
% Ef = 4.2;      % pbe NbS3
% Ef=2.884;   % MoTe2 no rotate

% Ef=-2.2513;   % MoTe2 no rotate

% Ef=-0.22;

print_bandgap(mat_file);

% fig_prop.Fontname='Arial';
% fig_prop.Fontsize=14;
fig_prop.Linewidth=2;
fig_prop.format='png';
% fig_prop.format='epsc';
fig_prop.dpi=200;

bs_prop.bs_flag='combined-sp';
bs_prop.Emin=-0.001;
bs_prop.Emax=0.001;
bs_prop.Emin=-0.01;
bs_prop.Emax=0.01;
bs_prop.Emin=-2;
bs_prop.Emax=2;
% bs_prop.Emin=-2.5;
% bs_prop.Emax=0.5;

% plot_bandstructure(mat_file, fig_prop, bs_prop)

% ---------------------- PLOT PROCAR ------------------------------
% supported modes: ionic, orbital, sigma-pi, t2g-eg and s-contrib
% For any mode specify energy range from Emin to Emax to plot
% ionic and orbital feature plots the ionic and orbital composition 
% sigma-pi plots the in-plane and out-of-plane contribution of p-orbital
% t2g-eg mode plots the contribution of t2g and eg modes of d-orbital
% s-contrib mode plots the contribution os s-orbital mode alone
% export features can be manipulated by fig_prop properties above.

procar_prop.procar_flag='ionic';
procar_prop.procar_flag='orbital';
procar_prop.procar_flag='sigma-pi';
procar_prop.procar_flag='t2g-eg';
procar_prop.procar_flag='s-contrib';
procar_prop.Emin=-0.001;
procar_prop.Emax=0.001;
procar_prop.Emin=-2;
procar_prop.Emax=2;
% procar_prop.Emin=-6;
% procar_prop.Emax=6;
% procar_prop.Emin=-0.5;
% procar_prop.Emax=0.5;
% procar_prop.Emin=0.8;
% % procar_prop.Emin=1;
% procar_prop.Emax=1.2;

plot_procar(mat_file, procar_prop, fig_prop)

dos_prop.dos_flag='total';
dos_prop.dos_flag='orbital';
% dos_prop.dos_flag='ionic';
% dos_prop.dos_flag='d-contrib';
% dos_prop.dos_flag='sp-total';
% dos_prop.dos_flag='sp-orbital';
% dos_prop.dos_flag='sp-ionic';
dos_prop.Emin=-2;
dos_prop.Emax=2.;
dos_prop.dos_max=1;
dos_prop.dos_max=30;
dos_prop.dos_max=5;
% dos_prop.dos_max=150;


% plot_doscar(mat_file, dos_prop, fig_prop);



% -------------------- m* CALCULATOR --------------------------------
% supported mode: generic
% three input are the most important. These are:
% mstar_prop.cut_th: this is the threshold in meV to cut off from the
% maxima or minima.
% mstar_prop.degrees: this is the vector with the order of polynomials to
% fit with.
% mstar_prop.split_th: for the spin-polarized or SOC case, this is the
% splitting threshold in meV. If the splitting is higher than the given
% cutoff, then the code will calculate m* of the band below VB and the band
% above CB.
% mstar.prop.dir_basis = dir_basis;
mstar_prop.mstar_mode='generic';
mstar_prop.mstar_mode='kp-gen';
% mstar_prop.mstar_mode='het-kp-gen';
% mstar_prop.mstar_dir='z-dir'; % default is in-plane
mstar_prop.mat_sym={'Mo'};
mstar_prop.write_mode='KPOINTS';
% mstar_prop.write_mode='mat_file';
mstar_prop.mstar_mode='mstar-calc';
mstar_prop.mstar_mode='het-mstar-calc';
mstar_prop.no_kpoints=31;
mstar_prop.data_save=true;
mstar_prop.cut_th = 60; % in meV
mstar_prop.degrees = [2 4 6];
mstar_prop.split_th=1; % in meV


% effective_mass(mat_file, mstar_prop, fig_prop);

% mstar;

% rec_basis
