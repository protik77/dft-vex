function mstar = effective_mass(all_data_mat_file, mstar_prop, fig_prop)

% constants
hbarc = 1973;       % eV A
mcsq = 0.511E6;     % eV

all_data_mat_path = fullfile(cd, all_data_mat_file);

load(all_data_mat_path, 'kpoints', 'procar_data', 'eigenvalue', 'Ef', ...
    'kpoints_wt', 'rec_basis', 'dir_basis', 'incar_tags', 'mat');

eigenvalue = eigenvalue-Ef;

[kpoints, eigenvalue, ~]=check_if_hse(true, incar_tags,...
    kpoints, kpoints_wt, eigenvalue, procar_data);

clear kpionts_wt procar_data;

rec_basis = rec_basis * 2 * pi;
spin_no = size(eigenvalue,1);
kpoints_rec=kpoints*rec_basis;
nokpoints = size(kpoints,1);
bands=size(eigenvalue,2);
SOC_FLAG=false; % initialize SOC_FLAG as false
% we already know for some orders of the polynomial the fit might be bad. 
% So will turn off this warning for the time being and 
% turn on at the end of the function.
warning_id = 'MATLAB:polyfit:RepeatedPointsOrRescale';
warning('off', warning_id);

cut_th=mstar_prop.cut_th; % cut-off to truncate band structure in meV
split_cut_off=mstar_prop.split_th; % in meV
degree=mstar_prop.degrees; % order of polynomial

cut_th = cut_th/1000; % converted to eV

[bg, vb_index, ~] = get_bandgap(eigenvalue); % get band index of VB

if bg <= 0
    warning('No band gap.')
end

% [kpath, ~] =  get_Kpath(mstar_prop.rec_basis, kpoints, false);

colors               = {
                        [ 0.16,     0.44,    1.00 ],...
                        [ 0.93,     0.00,    0.00 ],...
                        [ 0.00,     0.57,    0.00 ],...
                        [ 0.17,     0.17,    0.17 ],...
                        [ 0.44,     0.00,    0.99 ],...
                        [ 1.00,     0.50,    0.10 ],...
                        [ 0.75,     0.00,    0.75 ],...
                        [ 0.50,     0.50,    0.50 ],...
                        [ 0.50,     0.57,    0.00 ],...
                        [ 0.00,     0.00,    0.00 ]
                       };

switch mstar_prop.mstar_mode
    
    case 'generic' 
        
    % bandstructure calculation may have duplicate values. If it's in high
    % symmetry point, it may skew the value of the already not so correct m*
    % values.
    [ukpoints, ia,~] = unique(kpoints_rec, 'rows', 'stable');
    kpoints_rec = ukpoints;

    eigenvalue_temp = zeros(size(eigenvalue));

    for is = 1:spin_no
        eigenvalue_temp = eigenvalue(is,:,ia);            
    end

    eigenvalue = eigenvalue_temp;
    clear eigenvalue_temp ukpoints;

    [kpath, ~] =  get_Kpath(kpoints_rec, false);
    
    print_info = true;

    [ibands, vb_or_cb, spin_lim, flags] = get_indices(print_info, eigenvalue, incar_tags,...
        split_cut_off, vb_index, vb_index+1);

    % initialize all matrices
    mstar = zeros(spin_no, length(ibands), length(degree));
    poly_terms = zeros(spin_no, length(ibands), length(degree),degree(end)+1); 
    all_kpaths = nan * ones(spin_no, length(ibands), 50);
    all_bands = nan * ones(spin_no, length(ibands), 50);
    
    for is = 1:spin_lim
    
    for band_ind = 1:length(ibands)
            
        this_band = squeeze(eigenvalue(is, vb_index(is) + ibands(band_ind), :))';
        this_kpath = kpath;

        if vb_or_cb(band_ind) == 1
            % if it's vb, reverse the band
            this_band = -this_band;
        end
        
        % set the minima to zero
        [band_min, min_ind] = min(this_band);
        this_band = this_band - band_min;
        
        % set the center to zero
        this_kpath = this_kpath - kpath(min_ind);
        
        % variable min_ind is not needed anymore
        min_ind_temp = min_ind;
        clear min_ind;

        min_ind=find(this_band(1:min_ind_temp)>=cut_th,1,'last');
        max_ind=find(this_band(min_ind_temp:end)>=cut_th,1)+min_ind_temp-1;
        
        if (isempty(min_ind))
            min_ind=1;
        end

        if (isempty(max_ind))
            max_ind=length(this_band);
        end

        this_kpath = this_kpath(min_ind:max_ind);
        this_band  = this_band(min_ind:max_ind);
        
        all_kpaths(is, band_ind, 1:length(this_kpath)) = this_kpath;
        all_bands(is, band_ind, 1:length(this_band))= this_band;
        
        clear min_ind max_ind min_ind_temp;
        
        [band_min, ~] = min(this_band);  
        
        if vb_or_cb(band_ind) ==1
            fprintf(' Energy difference from VBM: %0.1fmeV\n\n', (max(this_band)-band_min)*1000);
            if ibands(band_ind) == -1
                vb_str = 'VB (-1)';
            elseif ibands(band_ind) == 0
                vb_str = 'VB';
            end
        else
            fprintf(' Energy difference from CBM: %0.1fmeV\n\n', (max(this_band)-band_min)*1000);
            if ibands(band_ind) == 1
                vb_str = 'CB';
            elseif ibands(band_ind) == 2
                vb_str = 'VB (+1)';
            end
        end
        
        if spin_no == 1
            spin_str='';
        elseif spin_no == 2 && is == 1
            spin_str='(UP)';
        elseif spin_no == 2 && is == 2
            spin_str='(DOWN)';
        end
        
        fprintf(' Effective mass of %s %s:\n',vb_str, spin_str);

        for id=1:length(degree)

        deg = degree(id);

        poly_terms(is,band_ind, id,1:deg+1) = polyfit(this_kpath, this_band, deg);
        
%         squeeze(poly_terms(is,band_ind, id,1:deg+1))

        mstar(is, band_ind, id) = hbarc * hbarc /2/mcsq/poly_terms(is, band_ind, id, deg-1);

        fprintf('\tUsing polynomial of order %d: %0.4f\n',...
            deg, mstar(is, band_ind, id));
        
        if id == length(degree)
            % like a break, maybe?
            fprintf('\n\n');
        end

        end

    end
        
    end
   

    case 'kp-gen'
        
        [~, vb_index, ~] = get_bandgap(eigenvalue); % get band index of VB        
        vb_cb_ind = [vb_index vb_index+1];
        k_write_frac = get_kpoints_to_write(mstar_prop, rec_basis, kpoints_rec, eigenvalue, vb_cb_ind);   
        write_kpoints_file(mstar_prop, k_write_frac);

    case 'het-kp-gen'
        
        het_vb_cb_ind = get_het_vb_cb_ind(all_data_mat_file, mstar_prop);
        k_write_frac = get_kpoints_to_write(mstar_prop, rec_basis, kpoints_rec, eigenvalue, het_vb_cb_ind);
        write_kpoints_file(mstar_prop, k_write_frac);
        
    case 'mstar-calc'
        
        [mstar, poly_terms, all_bands, all_kpaths] = ...
            calc_mstar(all_data_mat_file, mstar_prop);
        
    case 'het-mstar-calc'
        
        [mstar, poly_terms, all_bands, all_kpaths] = ...
            calc_mstar(all_data_mat_file, mstar_prop);

end

warning('on', warning_id);

% store data
if ~strcmp(mstar_prop.mstar_mode,'kp-gen') && ~strcmp(mstar_prop.mstar_mode,'het-kp-gen')...
        && mstar_prop.data_save
    
    if isfield(mstar_prop, 'mstar_dir') && strcmp(mstar_prop.mstar_dir,'z-dir')
        save('effective_mass_kz_data.mat','degree', 'mstar', 'poly_terms',...
            'all_kpaths', 'all_bands');  
    else
        save('effective_mass_data.mat','degree', 'mstar', 'poly_terms',...
            'all_kpaths', 'all_bands');    
    end
end
    
% plotting
if strcmp(mstar_prop.mstar_mode,'generic')

    if spin_no == 2 && spin_lim == 2
        figure('Units','inches','position',[1 1 10 8.5]);
    elseif spin_no == 2 && spin_lim == 1
        figure('Units','inches','position',[1 1 12 6]);
    elseif flags.soc_flag && flags.split_flag
        figure('Units','inches','position',[1 1 10 8.5]);
    else
        figure('Units','inches','position',[1 1 12 6]);
    end

    cnt=1;
    for is = 1:spin_lim

    for ib = 1:length(ibands)

    if vb_or_cb(ib) ==1
        vb_str = 'VB';
    else
        vb_str = 'CB';
    end

    no_nan_ind = sum(~isnan(squeeze(all_kpaths(is,ib,:))));

    x_poly = linspace(all_kpaths(is,ib,1),all_kpaths(is,ib, no_nan_ind),50);

    for id=1:length(degree)

    if spin_no == 2 && spin_lim == 2        
        fig1=subplot(2*length(ibands), length(degree), cnt);
    else        
        fig1=subplot(length(ibands), length(degree), cnt);
    end
    get_fig_obj(fig1, fig_prop, 'mstar');

    plot(x_poly, polyval(squeeze(poly_terms(is,ib,id,1:degree(id)+1)),x_poly)*1000,...
        'color', colors{1},'linewidth', fig_prop.Linewidth);
    plot(squeeze(all_kpaths(is,ib,1:no_nan_ind)), squeeze(all_bands(is,ib,1:no_nan_ind))*1000,...
        'x','color', colors{2},'linewidth', fig_prop.Linewidth, 'MarkerSize',10);

    xlabel('k (A^{-1})');
    ylim([0 (cut_th+(10/1000))*1000]); % limit for ylim is from 0 to (threshold + 10 meV)

    if spin_no == 1
        title(sprintf('%s, order: %d', vb_str, degree(id)))
    elseif spin_no == 2 && is ==1
        title(sprintf('%s, spin $\\uparrow$, order: %d', vb_str, degree(id)),'Interpreter','latex')
    elseif spin_no == 2 && is ==2
        title(sprintf('%s, spin $\\downarrow$, order: %d', vb_str, degree(id)),'Interpreter','latex')
    end


    if id ==1
        ylabel('Energy (meV)');
        legend('Fit','E-k', 'location','best')
    end

    cnt = cnt+1;

    end
    end
    end

elseif strcmp(mstar_prop.mstar_mode,'mstar-calc') || strcmp(mstar_prop.mstar_mode,'het-mstar-calc')
    
    if isfield(mstar_prop, 'mstar_mode') && strcmp(mstar_prop.mstar_mode,'het-mstar-calc')
        vb_cb_ind = get_het_vb_cb_ind(all_data_mat_file, mstar_prop);
    elseif isfield(mstar_prop, 'mstar_mode') && strcmp(mstar_prop.mstar_mode,'mstar-calc')
        [~, vb_index, ~] = get_bandgap(eigenvalue); % get band index of VB
        vb_cb_ind = [vb_index vb_index+1];
    else
        error(' mstar_mode not specified.');
    end
    
    if isfield(mstar_prop, 'mstar_dir') && strcmp(mstar_prop.mstar_dir,'z-dir')
        z_dir=true;
        idir_lim = 1; % if in z-dir, only kz
    else;z_dir = false;
        idir_lim = 2; % if not in z-dir, then kx and ky
    end
    
    print_info = false;
    
    [ibands, vb_or_cb, spin_lim, flags] = get_indices(print_info, eigenvalue, incar_tags,...
        split_cut_off, vb_cb_ind(1), vb_cb_ind(2));
    
    if ~z_dir
        fig_s1 = figure('Units','inches','position',[1 1 10 8.5]);
    else
        fig_s1 = figure('Units','inches','position',[1 1 10 5]);
    end

    flag_once=false;
    cnt=1;
    for is = 1:spin_lim

    for ib = 1:length(ibands)
        
    for idir = 1:idir_lim % along kx or ky

    if vb_or_cb(ib) ==1
        vb_str = 'VB';
    else
        vb_str = 'CB';
    end
    
    if idir_lim==2 && idir ==1
        dir_str = 'kx';
    elseif idir_lim==2 && idir ==2
        dir_str = 'ky';
    elseif idir_lim==1 && idir ==1
        dir_str = 'kz';
    else
        dir_str = '[fix it]';
    end

    no_nan_ind = sum(~isnan(squeeze(all_kpaths(is,ib,idir,:))));

    x_poly = linspace(all_kpaths(is,ib,idir,1),all_kpaths(is,ib,idir, no_nan_ind),50);

    for id=1:length(degree)

    if  spin_lim == 2 && is == 1
        fig1=subplot(2*length(ibands), length(degree), cnt);
        get_fig_obj(fig1, fig_prop, 'mstar');
    elseif is == 2 && ~flag_once
        cnt=1;
        fig_s2 = figure('Units','inches','position',[1 1 10 8.5]);
        fig2 = subplot(2*length(ibands), length(degree), cnt);
        get_fig_obj(fig2, fig_prop, 'mstar');
        flag_once=true;
    elseif is == 2 && ~flag_once
        fig2 = subplot(2*length(ibands), length(degree), cnt);
        get_fig_obj(fig2, fig_prop, 'mstar');
    elseif flags.soc_flag && flags.split_flag && ibands(ib) > 0 && ~flag_once
        cnt=1;
        fig_s2 = figure('Units','inches','position',[1 1 10 8.5]);
        fig2=subplot(length(ibands), length(degree), cnt);
        get_fig_obj(fig2, fig_prop, 'mstar');
        flag_once=true;
    elseif flags.soc_flag && flags.split_flag
        fig2=subplot(length(ibands), length(degree), cnt);
        get_fig_obj(fig2, fig_prop, 'mstar');
    elseif z_dir
        fig1=subplot(length(ibands), length(degree), cnt);
        get_fig_obj(fig1, fig_prop, 'mstar');
    else
        fig1=subplot(2*length(ibands), length(degree), cnt);
        get_fig_obj(fig1, fig_prop, 'mstar');
    end

    

    plot(x_poly, polyval(squeeze(poly_terms(is,ib,idir,id,1:degree(id)+1)),x_poly)*1000,...
        'color', colors{1},'linewidth', fig_prop.Linewidth);
    plot(squeeze(all_kpaths(is,ib,idir,1:no_nan_ind)), squeeze(all_bands(is,ib,idir,1:no_nan_ind))*1000,...
        'x','color', colors{2},'linewidth', fig_prop.Linewidth, 'MarkerSize',10);

    xlabel('k (A^{-1})');
    ylim([0 (cut_th+(10/1000))*1000]); % limit for ylim is from 0 to (threshold + 10 meV)

    if spin_no == 1 && ~flags.split_flag
        title(sprintf('%s, order: %d, dir: %s', vb_str, degree(id), dir_str))
        
    elseif spin_no == 1 && flags.split_flag
        if ib == 1
            title(sprintf('%s (-1), order: %d, dir: %s', vb_str, degree(id), dir_str))
        elseif ib == 4
            title(sprintf('%s (+1), order: %d, dir: %s', vb_str, degree(id), dir_str))
        else
            title(sprintf('%s, order: %d, dir: %s', vb_str, degree(id), dir_str))
        end
    elseif spin_no == 2 && is ==1
        title(sprintf('%s, spin $\\uparrow$, order: %d, dir: %s', vb_str, degree(id), dir_str),'Interpreter','latex')
    elseif spin_no == 2 && is ==2
        title(sprintf('%s, spin $\\downarrow$, order: %d, dir: %s', vb_str, degree(id), dir_sir),'Interpreter','latex')
    end


    if id ==1
        ylabel('Energy (meV)');
        legend('Fit','E-k', 'location','best')
    end

    cnt = cnt+1;

    end
    end
    end
    end


end

if isfield(mstar_prop, 'mstar_mode') && ~strcmp(mstar_prop.mstar_mode,'kp-gen') && ~strcmp(mstar_prop.mstar_mode,'het-kp-gen')

    fform=sprintf('-d%s', fig_prop.format);
    fdpi=sprintf('-r%d', fig_prop.dpi);

    if flags.split_flag
        print(fig_s1, 'effective_mass_fit_1',fform, fdpi);
        print(fig_s2, 'effective_mass_fit_2',fform, fdpi);
    else
        print('effective_mass_fit',fform, fdpi);
    end


end

end

function [ibands, vb_or_cb, spin_lim, flags] = get_indices(print_info, eigenvalue, incar_tags, split_cut_off, vb_index, cb_index)

flags.split_flag = false;
flags.soc_flag = false;
spin_no = size(eigenvalue,1);

if spin_no==2
    % spin polarized case
    vb_split = abs(max(eigenvalue(1,vb_index(1),:)) - max(eigenvalue(2,vb_index(2),:)));
    cb_split = abs(max(eigenvalue(1,cb_index(1)+1,:)) - max(eigenvalue(2,cb_index(2)+1,:))); 
    if print_info
        fprintf(' Spin polarized calculation and ');
    end

    % check for splitting
    if vb_split <= split_cut_off/1000 && cb_split <= split_cut_off/1000
        if print_info
            fprintf('splitting of VB and CB is less than %0.1fmeV.\n\n', split_cut_off);
        end
        ibands = [0 cb_index-vb_index];
        vb_or_cb = [1 0]; % 1 for vb 0 for cb
        spin_lim=1;
    else
        if print_info
            fprintf('splitting of VB or CB is larger than %0.1fmeV.\n\n', split_cut_off);
        end
        flags.split_flag = true;
        ibands = [-1 0 cb_index-vb_index cb_index-vb_index+1];
        vb_or_cb = [1 1 0 0]; % 1 for vb 0 for cb
        spin_lim=spin_no;
    end            

elseif spin_no==1 && strcmp(incar_tags.LSORBIT, 'T')
    % for SOC case
    vb_split = abs(max(eigenvalue(1,vb_index,:)) - max(eigenvalue(1,vb_index-1,:)));
    cb_split = abs(max(eigenvalue(1,cb_index,:)) - max(eigenvalue(1,cb_index+1,:)));
    flags.soc_flag=true; % strcmp is slower and creates a bottleneck
    if print_info
        fprintf(' SOC calculation and ');
    end
    spin_lim=1;

    % check for splitting
    if vb_split <= split_cut_off/1000 && cb_split <= split_cut_off/1000
        if print_info
            fprintf('splitting of VB and CB is less than %0.1fmeV.\n\n', split_cut_off);
        end
        ibands = [0 cb_index-vb_index];
        vb_or_cb = [1 0]; % 1 for vb 0 for cb
    else
        if print_info
            fprintf('splitting of VB or CB is larger than %0.1fmeV.\n\n', split_cut_off);
        end
        flags.split_flag = true;
        ibands = [-1 0 cb_index-vb_index cb_index-vb_index+1];
        vb_or_cb = [1 1 0 0]; % 1 for vb 0 for cb
    end

else
    % for every other case
    ibands = [0 cb_index-vb_index];
    vb_or_cb = [1 0]; % 1 for vb 0 for cb
    spin_lim=1;

end

end

function k_write_frac = get_kpoints_to_write(mstar_prop, rec_basis, kpoints_rec, eigenvalue, vb_cb_index)

    cut_th=mstar_prop.cut_th;
    cut_th = cut_th/1000;
    
    if isfield(mstar_prop, 'mstar_dir') && strcmp(mstar_prop.mstar_dir,'z-dir')
        z_dir = true;
    else
        z_dir = false;
    end

    % bandstructure calculation may have duplicate values. If it's in high
    % symmetry point, it may skew the value of the already not so correct m*
    % values.
    [ukpoints, ia,~] = unique(kpoints_rec, 'rows', 'stable');
    kpoints_rec = ukpoints;

    if ~isfield(mstar_prop, 'no_kpoints') 
        mstar_prop.no_kpoints=31; % default number of kpoints per line
    end

    eigenvalue_temp = zeros(size(eigenvalue));
    spin_no = size(eigenvalue,1);

    for is = 1:spin_no
        eigenvalue_temp = eigenvalue(is,:,ia);            
    end

    eigenvalue = eigenvalue_temp;
    clear eigenvalue_temp ukpoints;
    [kpath, ~] =  get_Kpath(kpoints_rec, false);
    cut_ind = zeros(2,2);
    dist = zeros(2,2);
    final_dist = zeros(1,2);
    
    if z_dir
        k_write_rec = zeros(2,2,3);
        k_write_frac = zeros(2,2,3);
    else
        k_write_rec = zeros(2,4,3);
        k_write_frac = zeros(2,4,3);
    end

    for ib=1:2 % vb and cb

        this_band = squeeze(eigenvalue(is, vb_cb_index(ib), :))';
        this_kpath = kpath;

        if ib == 1
            % if it's vb, reverse the band
            this_band = -this_band;
        end

        % set the minima to zero
        [band_min, min_ind] = min(this_band);
        this_band = this_band - band_min;

        % set the center to zero
        this_kpath = this_kpath - this_kpath(min_ind);

        left_temp = find(this_band(1:min_ind)>=cut_th,1,'last');
        right_temp = find(this_band(min_ind:end)>=cut_th,1);

        if isempty(left_temp)
            left_temp = 1;
        end

        if isempty(right_temp)
            right_temp = length(this_band);
        end

        cut_ind(ib,1)= left_temp;
        cut_ind(ib,2)= right_temp + min_ind-1;

        dist(ib,1) = this_kpath(min_ind) - this_kpath(cut_ind(ib,1));
        dist(ib,2) = this_kpath(cut_ind(ib,2)) - this_kpath(min_ind);

        final_dist(1, ib) = max(dist(ib,:));
        
        if z_dir
            k_write_rec(ib,1,:)=[kpoints_rec(min_ind,1) kpoints_rec(min_ind,2) kpoints_rec(min_ind,3) - final_dist(1,ib)];
            k_write_rec(ib,2,:)=[kpoints_rec(min_ind,1) kpoints_rec(min_ind,2) kpoints_rec(min_ind,3) + final_dist(1,ib)];
        else
            k_write_rec(ib,1,:)=[kpoints_rec(min_ind,1) - final_dist(1,ib) kpoints_rec(min_ind,2) kpoints_rec(min_ind,3)];
            k_write_rec(ib,2,:)=[kpoints_rec(min_ind,1) + final_dist(1,ib) kpoints_rec(min_ind,2) kpoints_rec(min_ind,3)];
            k_write_rec(ib,3,:)=[kpoints_rec(min_ind,1) kpoints_rec(min_ind,2) - final_dist(1,ib) kpoints_rec(min_ind,3)];
            k_write_rec(ib,4,:)=[kpoints_rec(min_ind,1) kpoints_rec(min_ind,2) + final_dist(1,ib) kpoints_rec(min_ind,3)];
        end

        k_write_frac(ib,:,:)=squeeze(k_write_rec(ib,:,:))*inv(rec_basis);

    end

end

function write_kpoints_file(mstar_prop, k_write_frac)

    if strcmp(mstar_prop.write_mode,'KPOINTS')
        
            if isfield(mstar_prop, 'mstar_dir') && strcmp(mstar_prop.mstar_dir,'z-dir')
                z_dir = true;
            else
                z_dir = false;
            end
            
        if z_dir
            filename = 'KPOINTS_z';
        else
            filename = 'KPOINTS';
        end
        
        fprintf(' Writing KPOINTS file.. ');

        fid=fopen(filename,'w');

        fprintf(fid,'kpoints file for effective mass calculation\n');
        fprintf(fid,'%d\n', mstar_prop.no_kpoints);
        fprintf(fid,'line\n');
        fprintf(fid,'Reciprocal\n');

        for ib=1:size(k_write_frac,1)
            for id=1:size(k_write_frac,2)

                fprintf(fid,'%12.8f %12.8f %12.8f ', k_write_frac(ib,id,1), ...
                    k_write_frac(ib,id,2), k_write_frac(ib, id,3));

                if ib == 1 && id == 2
                    fprintf(fid,'! VB logitudinal \n');
                elseif ib == 1 && id == 4
                    fprintf(fid,'! VB transverse \n');
                elseif ib == 2 && id == 2
                    fprintf(fid,'! CB logitudinal \n');
                elseif ib == 2 && id == 4
                    fprintf(fid,'! CB transverse \n');
                else
                    fprintf(fid,'\n');
                end

                if mod(id, 2) == 0
                    fprintf(fid,'\n');
                end
            end
        end

        logstr=sprintf('K-points for effective mass calculation is written in KPOINTS file.');

        WriteLog(logstr);

        fprintf('Done.\n\n');

    elseif strcmp(mstar_prop.write_mode,'mat_file')

        k_size = size(k_write_frac);

        kp_write_mat = zeros(k_size(1)*k_size(2),3);

        for ib=1:size(k_write_frac,1)
            kp_write_mat(((ib-1)*4)+1:ib*4, :) = squeeze(k_write_frac(ib,:,:));
        end

        save('effective_mass_kpoints.mat', 'kp_write_mat');  

    end

end

function [mstar, poly_terms, all_bands, all_kpaths] = calc_mstar(all_data_mat_file, mstar_prop)

    % constants
    hbarc = 1973;       % eV A
    mcsq = 0.511E6;     % eV

    all_data_mat_path = fullfile(cd, all_data_mat_file);

    load(all_data_mat_path);
    
    eigenvalue = eigenvalue-Ef;

    [kpoints, eigenvalue, ~]=check_if_hse(true, incar_tags,...
        kpoints, kpoints_wt, eigenvalue, procar_data);

    clear kpionts_wt procar_data;
    
    if isfield(mstar_prop, 'mstar_dir') && strcmp(mstar_prop.mstar_dir,'z-dir')
        z_dir = true;
    else
        z_dir = false;
    end    
    
    nokpoints = size(kpoints,1);
    bands=size(eigenvalue,2);
    cut_th=mstar_prop.cut_th; % cut-off to truncate band structure in meV
    degree=mstar_prop.degrees; % order of polynomial
    split_cut_off=mstar_prop.split_th; % in meV
    spin_no = size(eigenvalue,1);

    rec_basis = rec_basis * 2 * pi;
    
    kpoints_rec=kpoints*rec_basis;

    [kpath, ~] =  get_Kpath(kpoints_rec, false);
    
    if isfield(mstar_prop, 'mstar_mode') && strcmp(mstar_prop.mstar_mode,'het-mstar-calc')
        vb_cb_ind = get_het_vb_cb_ind(all_data_mat_file, mstar_prop);
    elseif isfield(mstar_prop, 'mstar_mode') && strcmp(mstar_prop.mstar_mode,'mstar-calc')
        [~, vb_index, ~] = get_bandgap(eigenvalue); % get band index of VB
        vb_cb_ind = [vb_index vb_index+1];
    else
        error(' mstar_mode not specified.');
    end
    
    print_info = true;
    
    [ibands, vb_or_cb, spin_lim, ~] = get_indices(print_info, eigenvalue, incar_tags,...
        split_cut_off, vb_cb_ind(1), vb_cb_ind(2));

    old_size = size(eigenvalue);
    
    if z_dir 
        sec = 2;
    else
        sec = 4; 
    end
    
    kp_per_sec = nokpoints/sec;
    
    new_size = [old_size(1), sec, old_size(2), old_size(3)/sec];
    eigenvalue2 = zeros(new_size);
    kp_sec_ind = zeros(spin_lim, sec,2);
    


    for is = 1:spin_lim
    for isec = 1:sec

        beg_ind = (isec-1)*kp_per_sec +1;
        end_ind = isec*kp_per_sec;

        kp_sec_ind(is, isec, :) = [beg_ind, end_ind];

        eigenvalue2(is, isec, :, :) = ...
            squeeze(eigenvalue(is, :, beg_ind:end_ind));

    end
    end

    clear eigenvalue;

    eigenvalue = eigenvalue2;

    clear eigenvalue2;
    
    % initialize all matrices
    mstar = zeros(spin_no, length(ibands), sec/2, length(degree));
    poly_terms = zeros(spin_no, length(ibands), sec/2, length(degree),degree(end)+1); 
    all_kpaths = nan * ones(spin_no, length(ibands), sec/2, 50);
    all_bands = nan * ones(spin_no, length(ibands), sec/2, 50);

    for is = 1:spin_lim

    for band_ind = 1:length(ibands)

    direction_ind = 1;
    
    for isec = 1:sec

    if ( ~z_dir && (vb_or_cb(band_ind)==1 && isec < 3) || (vb_or_cb(band_ind)== 0 && isec > 2))...
            || (z_dir && isec ==1)

        this_band = squeeze(eigenvalue(is, isec, vb_cb_ind(is) + ibands(band_ind),:))';
        this_kpath = kpath(kp_sec_ind(is,isec,1):kp_sec_ind(is,isec,2));

        if vb_or_cb(band_ind) == 1
            % if it's vb, reverse the band
            this_band = -this_band;
        end

        % set the minima to zero
        [band_min, min_ind] = min(this_band);
        this_band = this_band - band_min;

        % set the center to zero
        this_kpath = this_kpath - this_kpath(min_ind);

        % variable min_ind is not needed anymore
        min_ind_temp = min_ind;
        clear min_ind;            

        min_ind=find(this_band(1:min_ind_temp)>=cut_th,1,'last');
        max_ind=find(this_band(min_ind_temp:end)>=cut_th,1)+min_ind_temp-1;

        if (isempty(min_ind))
            min_ind=1;
        end

        if (isempty(max_ind))
            max_ind=length(this_band);
        end

        this_kpath = this_kpath(min_ind:max_ind);
        this_band  = this_band(min_ind:max_ind);            

        all_kpaths(is, band_ind, direction_ind, 1:length(this_kpath)) = this_kpath;
        all_bands(is, band_ind, direction_ind, 1:length(this_band))= this_band;

        clear min_ind max_ind min_ind_temp;

        [band_min, ~] = min(this_band);  

        if vb_or_cb(band_ind) ==1
            fprintf(' Energy difference from VBM: %0.1fmeV\n\n', (max(this_band)-band_min)*1000);
            if ibands(band_ind) == -1
                vb_str = 'VB (-1)';
            elseif ibands(band_ind) == 0
                vb_str = 'VB';
            end
        else
            fprintf(' Energy difference from CBM: %0.1fmeV\n\n', (max(this_band)-band_min)*1000);
            if vb_or_cb(band_ind) == 0 && band_ind ==2
                vb_str = 'CB';
            elseif band_ind > 3
                vb_str = 'CB (+1)';
            end
        end

        if spin_no == 1
            spin_str='';
        elseif spin_no == 2 && is == 1
            spin_str='(UP)';
        elseif spin_no == 2 && is == 2
            spin_str='(DOWN)';
        end

        if z_dir
            k_dir_str = 'kz';
        else
            if isec == 1 || isec == 3
                k_dir_str = 'kx';
            else
                k_dir_str = 'ky';
            end
        end

        fprintf(' Effective mass of %s along %s direction %s:\n',vb_str, k_dir_str, spin_str);

        for id=1:length(degree)

        deg = degree(id);

        poly_terms(is,band_ind, direction_ind, id,1:deg+1) = polyfit(this_kpath, this_band, deg);

    %         squeeze(poly_terms(is,band_ind, id,1:deg+1))

        mstar(is, band_ind, direction_ind, id) = ...
            hbarc * hbarc /2/mcsq/poly_terms(is, band_ind, direction_ind, id, deg-1);

        fprintf('\tUsing polynomial of order %d: %0.4f\n',...
            deg, mstar(is, band_ind, direction_ind, id));

        if id == length(degree)
            % like a break, maybe?
            fprintf('\n\n');
        end

        end
        

        direction_ind = direction_ind+1;

    end

    end

    end

    end
    

end

