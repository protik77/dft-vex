function [plane_sum, grand_sum, ion_data, orbital_data, Total_Orbital] = procar_sum(all_data_mat_file, procar_data_file)

all_data_mat_path = fullfile(cd,all_data_mat_file);
load(all_data_mat_path);

eigenvalues = eigenvalue-Ef;

[kpoints, eigenvalues, procar_data]=check_if_hse(true, incar_tags,...
    kpoints, kpoints_wt, eigenvalues, procar_data);

    
nokpoints = size(kpoints,1);
orbital = cellstr(orbital_list);
bands=size(eigenvalues,2);
ions_no = size(procar_data,4);
spin_no = size(procar_data,3);

for ii=1:length(mat.mat_no)
    ATOM_GROUP(ii)=mat.mat_no(ii);
    ATOM_TAG{ii}=mat.mat_sym(ii,:);
end

a=1;
for ig=1:size(ATOM_GROUP,2)
    gv=ATOM_GROUP(ig);    
    for i=1:gv        
        ATOM_seq{a}=ATOM_TAG{ig};
        a=a+1;
    end    
end
clear a ig gv;

% this should work for both spin and no-spin plarized cases
if spin_no==2
    sp_spin=2;
else
    sp_spin=1;
end

orbital_length = length(orbital);

% optimization
spd_flag=zeros(1, orbital_length);
for io=1:orbital_length
    if (strncmpi(char(orbital{io}), 's',1))
        spd_flag(io)=1;
    elseif (strncmpi(char(orbital{io}), 'p',1))
        spd_flag(io)=2;
    elseif (strncmpi(char(orbital{io}), 'd',1)) || (strncmpi(char(orbital{io}), 'x',1))
        spd_flag(io)=3;
    else
        error('Error is orbital idenfication part.')
    end
end


orbital_data=zeros(sp_spin, nokpoints, bands, ions_no,3);
for is=1:sp_spin
for ik=1:nokpoints
for ib=1:bands
for io=1:orbital_length
    if spd_flag(io)==1
        orbital_data(is, ik, ib, :, 1) = orbital_data(is, ik,ib,:, 1) + procar_data(ik,ib,is,:, io);
    elseif spd_flag(io)==2
        orbital_data(is, ik, ib, :, 2) = orbital_data(is, ik,ib,:, 2) + procar_data(ik,ib,is,:, io);
    elseif spd_flag(io)==3
        orbital_data(is, ik, ib, :, 3) = orbital_data(is, ik,ib,:, 3) + procar_data(ik,ib,is,:, io);
    else
        error('Something is wrong.');
    end
end
end
end
end

% % optimized and vectorized
% permute(procar_data,[3 1 2 4 5]);
% orbital_data=zeros(sp_spin, nokpoints, bands, ions_no,3);
% for io=1:orbital_length
%     if spd_flag(io)==1
%         orbital_data(:, :, :, :, 1)= (orbital_data(:, :,:,:, 1))+(procar_data(:,:,:,:, io));
%     elseif spd_flag(io)==2
%         orbital_data(:, :, :, :, 2)= (orbital_data(:, :,:,:, 2))+(procar_data(:,:,:,:, io));
%     elseif spd_flag(io)==3
%         orbital_data(:, :, :, :, 3)= (orbital_data(:, :,:,:, 3))+(procar_data(:,:,:,:, io));
%     else
%         error('Something is wrong.');
%     end
% end


final_orbital_no=3;
sum2=zeros(1,ions_no);
s_contribution=zeros(sp_spin,nokpoints,bands,ions_no);
p_contribution=zeros(sp_spin,nokpoints,bands,ions_no);
d_contribution=zeros(sp_spin,nokpoints,bands,ions_no);
ion_data=zeros(sp_spin,nokpoints,bands,ions_no);
Total_Orbital=zeros(sp_spin,nokpoints,bands,final_orbital_no);
grand_sum=zeros(sp_spin,nokpoints, bands);

for is=1:sp_spin
for ik=1:nokpoints
for ib=1:bands
   ssum=sum(orbital_data(is,ik,ib,:,1));
   psum=sum(orbital_data(is,ik,ib,:,2));
   dsum=sum(orbital_data(is,ik,ib,:,3));

   sum1=ssum+psum+dsum;

    s_contribution(is,ik,ib,:) = orbital_data(is,ik,ib,:,1)/ssum;

    p_contribution(is,ik,ib,:) = orbital_data(is,ik,ib,:,2)/psum;

    d_contribution(is,ik,ib,:) = orbital_data(is,ik,ib,:,3)/dsum;

    for ii=1:ions_no
        sum2(ii)=sum(orbital_data(is,ik,ib,ii,:));
        grand_sum(is,ik,ib)=grand_sum(is,ik,ib)+sum(orbital_data(is,ik,ib,ii,:));
    end

    ion_data(is,ik,ib,:) = sum2./sum1;

    Total_Orbital(is,ik,ib,1) = ssum/sum1;
    Total_Orbital(is,ik,ib,2) = psum/sum1;
    Total_Orbital(is,ik,ib,3) = dsum/sum1;
end
end
end

% first 3 is for s, p and, d orbital
% second 2 is for in-plane and out-of-plane
plane_sum = zeros(sp_spin,3,2,bands,nokpoints);

% orbitals=       {s py pz px dxy dyz dz2 dxz dx2}
p_in_plane=       [0  1  0  1  0   0   0   0   0];
p_out_of_plane=   [0  0  1  0  0   0   0   0   0];
d_eg=             [0  0  0  0  0   0   1   0   1];
d_t2g=            [0  0  0  0  1   1   0   1   0];

for is=1:sp_spin
for ib = 1:bands    
    for ii = 1:ions_no
        for io = 1:orbital_length
            
            if io ==1
                plane_sum(is, 1, 1, ib, :) = squeeze(plane_sum(is, 1, 1, ib, :)) + procar_data(:,ib,is,ii, io);
            end
            
            if(p_in_plane(io)==1)
                plane_sum(is, 2, 1, ib, :) = squeeze(plane_sum(is, 2, 1, ib, :)) + procar_data(:,ib,is,ii, io);
            end
            
            if(p_out_of_plane(io)==1)
                plane_sum(is, 2, 2, ib, :) = squeeze(plane_sum(is, 2, 2, ib, :)) + procar_data(:,ib,is,ii, io);
            end
            
            if(d_eg(io)==1)
                plane_sum(is, 3, 1, ib, :) = squeeze(plane_sum(is, 3, 1, ib, :)) + procar_data(:,ib,is,ii, io);
            end
            
            if(d_t2g(io)==1)
                plane_sum(is, 3, 2, ib, :) = squeeze(plane_sum(is, 3, 2, ib, :)) + procar_data(:,ib,is,ii, io);
            end
            
        end
    end    
end
end

fprintf(' Saving procar data to %s file.\n\n', procar_data_file);
save(procar_data_file, 'plane_sum', 'grand_sum', 'ion_data', 'orbital_data', 'Total_Orbital')

% end

end